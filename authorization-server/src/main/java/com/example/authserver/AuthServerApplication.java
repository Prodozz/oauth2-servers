package com.example.authserver;

import com.example.authserver.DTOs.customerDTOs.RegisterCustomerDTO;
import com.example.authserver.config.UserAuthority;
import com.example.authserver.entities.CustomerEntity;
import com.example.authserver.enums.Gender;
import com.example.authserver.enums.PaymentMethod;
import com.example.authserver.enums.Status;
import com.example.authserver.repositories.CustomerCRUDRepository;
import com.example.authserver.security.scopes.CustomOidcScopes;
import com.example.authserver.services.CustomerUserService;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.core.AuthorizationGrantType;
import org.springframework.security.oauth2.core.ClientAuthenticationMethod;
import org.springframework.security.oauth2.core.oidc.OidcScopes;
import org.springframework.security.oauth2.server.authorization.client.RegisteredClient;
import org.springframework.security.oauth2.server.authorization.client.RegisteredClientRepository;
import org.springframework.security.oauth2.server.authorization.settings.ClientSettings;
import org.springframework.security.oauth2.server.authorization.settings.OAuth2TokenFormat;
import org.springframework.security.oauth2.server.authorization.settings.TokenSettings;
import org.springframework.security.provisioning.UserDetailsManager;

import java.time.Duration;
import java.util.List;
import java.util.UUID;

import static com.example.authserver.config.UserAuthority.ADMIN;
import static com.example.authserver.config.UserAuthority.CUSTOMER;
import static org.springframework.security.core.userdetails.User.withUsername;

@SpringBootApplication
@RequiredArgsConstructor
public class AuthServerApplication implements CommandLineRunner {

    private final RegisteredClientRepository registeredClientRepository;
    private final PasswordEncoder passwordEncoder;
    private final UserDetailsManager userDetailsManager;
    private final CustomerCRUDRepository customerCRUDRepository;


    public static void main(String[] args) {
        SpringApplication.run(AuthServerApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {

        RegisteredClient registeredClient = RegisteredClient.withId(UUID.randomUUID().toString())
                .clientId("messaging-client")
                .clientSecret(passwordEncoder.encode("{noop}secret"))
                .clientAuthenticationMethod(ClientAuthenticationMethod.CLIENT_SECRET_BASIC)
                .authorizationGrantType(AuthorizationGrantType.AUTHORIZATION_CODE)
                .authorizationGrantType(AuthorizationGrantType.REFRESH_TOKEN)
                .authorizationGrantType(AuthorizationGrantType.CLIENT_CREDENTIALS)
                .redirectUri("http://127.0.0.1:5173/login/oauth2/code/messaging-client-oidc")
                .redirectUri("http://127.0.0.1:5173/authorized")
                .redirectUri("http://localhost:5173/authorized")
                .scope(OidcScopes.OPENID)
                .scope(OidcScopes.PROFILE)
                .scope(OidcScopes.ADDRESS)
                .scope(OidcScopes.EMAIL)
                .scope(OidcScopes.PHONE)
                .scope(CustomOidcScopes.PAYMENT)
                .clientSettings(ClientSettings.builder().requireAuthorizationConsent(false).build())
                .tokenSettings(
                        TokenSettings.builder()
                                .accessTokenFormat(OAuth2TokenFormat.SELF_CONTAINED)
                                .reuseRefreshTokens(false)
                                .accessTokenTimeToLive(Duration.ofDays(10))
                                .build()
                )
                .build();
        registeredClientRepository.save(registeredClient);


        userDetailsManager.createUser(
                withUsername("Raul")
                        .password(passwordEncoder.encode("12345"))
                        .authorities(List.of(ADMIN))
                        .build()
        );
        userDetailsManager.createUser(
                withUsername("CUSTOMER")
                        .password(passwordEncoder.encode("12345"))
                        .authorities(List.of(CUSTOMER))
                        .build()
        );

        userDetailsManager.createUser(
                withUsername("Mansur")
                        .password(passwordEncoder.encode("12345"))
                        .authorities(List.of(ADMIN))
                        .build()
        );

        userDetailsManager.createUser(
                withUsername("Stephan")
                        .password(passwordEncoder.encode("12345"))
                        .authorities(List.of(ADMIN))
                        .build()
        );
        userDetailsManager.createUser(
                withUsername("ADMIN")
                        .password(passwordEncoder.encode("12345"))
                        .authorities(List.of(ADMIN))
                        .build()
        );


        customerCRUDRepository.save(new CustomerEntity(
                "Mansur",
                "man.bibulatov@gmail.com",
                "12345",
                true,
                List.of(ADMIN),
                true,
                "252435",
                true,
                "11-04-2001",
                "Mansur",
                "Bibulatov",
                Gender.MALE,
                UUID.randomUUID(),
                PaymentMethod.MASTERCARD,
                Status.NOT_VERIFIED,
                true
        ));


    }
}
