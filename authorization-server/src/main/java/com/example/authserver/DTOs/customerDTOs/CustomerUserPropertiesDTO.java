package com.example.authserver.DTOs.customerDTOs;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CustomerUserPropertiesDTO {

    private String username;
    private String phoneNumber;
    private String birthDate;
    private String firstName;
    private String lastName;
    private String email;
}
