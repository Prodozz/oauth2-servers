package com.example.authserver.DTOs.customerDTOs;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class FullCustomerDTO {

    private String customerId;
    private String username;
    private String phoneNumber;
    private String birthDate;
    private String firstName;
    private String lastName;
    private String status;
    private String paymentMethod;
    private Boolean hasCreditWorthiness;
    private String addressId;

}
