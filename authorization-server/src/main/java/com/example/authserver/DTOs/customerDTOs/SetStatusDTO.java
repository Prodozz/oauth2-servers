package com.example.authserver.DTOs.customerDTOs;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SetStatusDTO {

    private String customerId;
    private String status;
    private Boolean hasCreditWorthiness;

}
