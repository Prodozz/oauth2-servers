package com.example.authserver.DTOs.employeeDTOs;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@Data
public class FullEmployeeDTO {

    private String employeeId;
    private String username;
    private String phoneNumber;
    private String birthDate;
    private String firstName;
    private String lastName;
    private Double grossSalary;
    private String employmentType;
    private String position;

}
