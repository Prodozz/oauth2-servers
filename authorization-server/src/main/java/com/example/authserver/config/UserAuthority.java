package com.example.authserver.config;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.security.core.GrantedAuthority;

@AllArgsConstructor
@Getter
public enum UserAuthority implements GrantedAuthority {

    CUSTOMER("ROLE_CUSTOMER"),
    EMPLOYEE("ROLE_EMPLOYEE"),
    ADMIN("ROLE_ADMIN");

    private final String authority;

    @Override
    public String getAuthority() {
        return authority;
    }

    public static UserAuthority fromStringToAuthority(String authorityString) {
        for (UserAuthority authority : UserAuthority.values()) {
            if (authority.getAuthority().equals(authorityString)) {
                return authority;
            }
        }
        throw new IllegalArgumentException("Error!");
    }
}
