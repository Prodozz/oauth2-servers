package com.example.authserver.config;

import java.util.UUID;

public interface UserI {

    UUID getId();
    String getFirstName();
    String getLastName();
    String getPhoneNumber();
    String getUsername();

}
