package com.example.authserver.controllers;

import com.example.authserver.DTOs.customerDTOs.CustomerUserPropertiesDTO;
import com.example.authserver.DTOs.customerDTOs.RegisterCustomerDTO;
import com.example.authserver.DTOs.customerDTOs.SetPaymentDTO;
import com.example.authserver.DTOs.customerDTOs.SetStatusDTO;
import com.example.authserver.services.CustomerService;
import com.example.authserver.services.CustomerUserService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.web.bind.annotation.*;


import static org.springframework.http.HttpStatus.BAD_REQUEST;

@RestController
@CrossOrigin
@RequestMapping(path = "/api/customer")
@AllArgsConstructor
public class CustomerController {

    final CustomerService customerService;
    final UserDetailsManager userDetailsManager;
    final CustomerUserService customerUserService;

    @GetMapping("/getAll")
    @PreAuthorize("@customMethodSecurity.hasAnyRoleAuthority('ADMIN','EMPLOYEE')")
    public ResponseEntity<?> getAllCustomers() {
        try {
            return ResponseEntity.ok(customerService.getAllCustomers());
        } catch (Exception e) {
            return new ResponseEntity<>(e, BAD_REQUEST);
        }
    }

    @GetMapping("/{customerId}")
    @PreAuthorize("@customMethodSecurity.hasAnyRoleAuthority('ADMIN','EMPLOYEE')")
    public ResponseEntity<?> getCustomer(@PathVariable String customerId) {
        try {
            return ResponseEntity.ok(customerService.getCustomer(customerId));
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), BAD_REQUEST);
        }
    }

    //TODO: fix getting username
    @GetMapping
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<?> getCustomerPropertiesByUsername() {
        try {
            return ResponseEntity.ok(customerService.getCustomerPropertiesByUsername());
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), BAD_REQUEST);
        }
    }

    @PostMapping("/register")
    @PreAuthorize("permitAll()")
    public ResponseEntity<?> createCustomer(@RequestBody RegisterCustomerDTO registerCustomerDTO) {
        try {
            customerUserService.createCustomerUser(registerCustomerDTO);
            return ResponseEntity.ok(customerService.getCustomerPropertiesByDTO(registerCustomerDTO));
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), BAD_REQUEST);
        }
    }

    @PutMapping
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<?> updateCustomerProperties(@RequestBody CustomerUserPropertiesDTO customerUserPropertiesDTO) {
        try {
            customerService.updateCustomerPropertiesByUsername(customerUserPropertiesDTO);
            return ResponseEntity.ok(customerService.getCustomerPropertiesByUsername());
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), BAD_REQUEST);
        }
    }


    @PutMapping("/setPaymentMethod")
    @PreAuthorize("@customMethodSecurity.hasAnyRoleAuthority('ADMIN','EMPLOYEE')")
    public ResponseEntity<?> setCustomerPayment(@RequestBody SetPaymentDTO paymentMethod) {
        try {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            customerService.setPayment(paymentMethod);
            return ResponseEntity.ok(customerService.getCustomer(authentication.getName()));
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), BAD_REQUEST);
        }
    }

    @PutMapping("/setStatus")
    @PreAuthorize("@customMethodSecurity.hasAnyRoleAuthority('ADMIN','EMPLOYEE')")
    public ResponseEntity<?> setCustomerStatus(@RequestBody SetStatusDTO setStatusDTO) {
        try {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            customerService.setStatus(setStatusDTO);
            return ResponseEntity.ok(customerService.getCustomer(authentication.getName()));
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), BAD_REQUEST);
        }
    }

    // TODO: paymentstatus mit hasCreditWorthiness RestMethode entweder in CustomerController oder ContractPaymentStatus


}
















