package com.example.authserver.controllers;

import com.example.authserver.DTOs.employeeDTOs.EmployeeUserPropertiesDTO;
import com.example.authserver.DTOs.employeeDTOs.RegisterEmployeeDTO;
import com.example.authserver.repositories.EmployeeCRUDRepository;
import com.example.authserver.services.EmployeeService;
import com.example.authserver.services.EmployeeUserService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.parameters.P;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.HttpStatus.BAD_REQUEST;

@RestController
@CrossOrigin
@RequestMapping(path = "/api/employee")
@AllArgsConstructor
public class EmployeeController {

    final EmployeeService employeeService;
    final EmployeeUserService employeeUserService;
    final EmployeeCRUDRepository employeeCRUDRepository;

    @GetMapping("/getAll")
    @PreAuthorize("@customMethodSecurity.isAdmin()")
    public ResponseEntity<?> getAllEmployees() {
        try {
            return ResponseEntity.ok(employeeService.getAllEmployees());
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), BAD_REQUEST);
        }
    }

    @GetMapping("/{employeeId}")
    @PreAuthorize("@customMethodSecurity.hasAnyRoleAuthority('EMPLOYEE','ADMIN')")
    public ResponseEntity<?> getEmployee(@PathVariable String employeeId) {
        try {
            return ResponseEntity.ok(employeeService.getEmployee(employeeId));
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), BAD_REQUEST);
        }
    }

    @GetMapping("/credentials/{employeeId}")
    @PreAuthorize("@customMethodSecurity.isAdmin()")
    public ResponseEntity<?> getEmployeeProperties(@PathVariable String employeeId) {
        try {
            return ResponseEntity.ok(employeeService.getEmployeePropertiesById(employeeId));
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), BAD_REQUEST);
        }
    }

    @PostMapping("/register")
    @PreAuthorize("@customMethodSecurity.isAdmin()")
    public ResponseEntity<?> createEmployee(@RequestBody RegisterEmployeeDTO registerEmployeeDTO) {
        try {
            employeeUserService.createEmployeeUser(registerEmployeeDTO);
            return ResponseEntity.ok(employeeService.getEmployeePropertiesByDTO(registerEmployeeDTO.getUsername()));
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), BAD_REQUEST);
        }
    }

    @PutMapping
    @PreAuthorize("authentication.name == #employeeUserPropertiesDTO.username && isAuthenticated()")
    public ResponseEntity<?> updateEmployeeProperties(@RequestBody EmployeeUserPropertiesDTO employeeUserPropertiesDTO) {
        try {
            employeeService.updateEmployeeProperties(employeeUserPropertiesDTO);
            return ResponseEntity.ok(employeeService.getEmployeePropertiesById(employeeUserPropertiesDTO.getEmployeeId()));
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), BAD_REQUEST);
        }
    }




}
















