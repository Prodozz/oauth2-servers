package com.example.authserver.controllers;

import com.example.authserver.DTOs.userDTOs.ChangePasswordDTO;
import com.example.authserver.services.CustomerUserService;
import com.example.authserver.services.EmployeeUserService;
import com.example.authserver.services.UserEntityService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.web.bind.annotation.*;

import static javax.security.auth.callback.ConfirmationCallback.OK;
import static org.springframework.http.HttpStatus.BAD_REQUEST;

@RestController
@CrossOrigin
@RequestMapping(path = "/api")
@AllArgsConstructor
public class UserController {


    private EmployeeUserService employeeUserService;
    private CustomerUserService customerUserService;
    private UserDetailsManager userDetailsManager;
    private UserEntityService userEntityService;


    @DeleteMapping("/employee/delete/{employeeId}")
    @PreAuthorize("@customMethodSecurity.isAdmin()")
    public ResponseEntity<?> deleteEmployee(@PathVariable String employeeId) {
        try {
            employeeUserService.deleteEmployeeUser(employeeId);
            return ResponseEntity.ok("Deleted!");
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), BAD_REQUEST);
        }
    }

//    @DeleteMapping("/customer/delete")
//    @PreAuthorize("isAuthenticated()")
//    public ResponseEntity<?> deleteCustomer() {
//        try {
//            String username = SecurityContextHolder.getContext().getAuthentication().getName();
//            customerUserService.deleteCustomerByUsername(username);
//            return ResponseEntity.ok("Deleted!");
//        } catch (Exception e) {
//            return new ResponseEntity<>(e.getMessage(), BAD_REQUEST);
//        }
//    }

    @DeleteMapping("/customer/delete/{customerId}")
    @PreAuthorize("hasAnyRole('ADMIN','EMPLOYEE')")
    public ResponseEntity<?> deleteCustomer(@PathVariable String customerId) {
        try {
            customerUserService.deleteCustomerById(customerId);
            return ResponseEntity.ok("Deleted!");
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), BAD_REQUEST);
        }
    }



    @PutMapping("/user/changePassword")
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<?> changePassword(@RequestBody ChangePasswordDTO changePasswordDTO) {
        try {
            userDetailsManager.changePassword(changePasswordDTO.getOldPassword(), changePasswordDTO.getNewPassword());
            return ResponseEntity.ok(OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), BAD_REQUEST);
        }
    }

//    @PutMapping("/user/updateEmployee")
//    @PreAuthorize("isAuthenticated()")
//    public ResponseEntity<?> updateEmployee(@RequestBody UserDetailsDTO userDetailsDTO) {
//        try {
//            userDetailsManager.updateUser(userEntityService.createSecurityEmployee(userDetailsDTO));
//            return ResponseEntity.ok(HttpStatus.OK);
//        } catch (Exception e) {
//            return new ResponseEntity<>(e.getMessage(), BAD_REQUEST);
//        }
//    }


//    @PutMapping("/user/updateCustomer")
//    @PreAuthorize("principal.username == #userDetailsDTO.username && isAuthenticated()")
//    public ResponseEntity<?> updateCustomer(@RequestBody UserDetailsDTO userDetailsDTO) {
//        try {
//            userDetailsManager.updateUser(userEntityService.createSecurityCustomer(userDetailsDTO));
//            return ResponseEntity.ok(HttpStatus.OK);
//        } catch (Exception e) {
//            return new ResponseEntity<>(e.getMessage(), BAD_REQUEST);
//        }
//    }

}
