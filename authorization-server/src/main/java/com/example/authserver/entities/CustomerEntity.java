package com.example.authserver.entities;

import com.example.authserver.config.UserAuthority;
import com.example.authserver.config.UserI;
import com.example.authserver.enums.Gender;
import com.example.authserver.enums.PaymentMethod;
import com.example.authserver.enums.Status;
import jakarta.persistence.*;
import lombok.*;

import java.util.List;
import java.util.UUID;

@Entity
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CustomerEntity extends UserEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private UUID customerId;
    @Enumerated(EnumType.STRING)
    private PaymentMethod paymentMethod;
    @Enumerated(EnumType.STRING)
    private Status status;
    private Boolean hasCreditWorthiness;


    public CustomerEntity(String username, String password, Boolean enabled, List<UserAuthority> authority) {
        super(username, password, enabled, authority);
    }
    public CustomerEntity(UUID customerId, String lastName, String firstName, String birthDate, String phoneNumber ){
        super(lastName, firstName, birthDate, phoneNumber);
        this.customerId = customerId;

    }

    public CustomerEntity(String username, String email, String password, Boolean enabled, List<UserAuthority> authority,
                          Boolean emailVerified, String phoneNumber, Boolean phoneNumberVerified, String birthDate,
                          String firstName, String lastName, Gender gender, UUID customerId, PaymentMethod paymentMethod,
                          Status status, Boolean hasCreditWorthiness) {


        super(username, email, password, enabled, authority, emailVerified, phoneNumber, phoneNumberVerified,
                birthDate, firstName, lastName, gender);
        this.customerId = customerId;
        this.paymentMethod = paymentMethod;
        this.status = status;
        this.hasCreditWorthiness = hasCreditWorthiness;
    }
}
