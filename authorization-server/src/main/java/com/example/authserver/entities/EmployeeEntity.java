package com.example.authserver.entities;

import com.example.authserver.config.UserAuthority;
import com.example.authserver.config.UserI;
import com.example.authserver.enums.*;
import com.example.authserver.config.UserI;
import com.example.authserver.enums.Employment_Type;
import com.example.authserver.enums.Position;
import jakarta.persistence.*;
import lombok.*;

import java.util.List;
import java.util.UUID;

@Entity
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class EmployeeEntity extends UserEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private UUID employeeId;
    private Double grossSalary;
    @Enumerated(EnumType.STRING)
    private Employment_Type employmentType;
    @Enumerated(EnumType.STRING)
    private Position position;


    public EmployeeEntity(String username, String password, Boolean enabled, List<UserAuthority> authority) {
        super(username, password, enabled, authority);
    }

    public EmployeeEntity(UUID employeeId, String lastName, String firstName, String birthDate, String phoneNumber,
    Double grossSalary, Employment_Type employmentType, Position position){
        super(lastName, firstName, birthDate, phoneNumber);
        this.employeeId = employeeId;
        this.grossSalary = grossSalary;
        this.employmentType = employmentType;
        this.position = position;

    }



}
