package com.example.authserver.entities;

import com.example.authserver.config.UserAuthority;
import com.example.authserver.enums.Gender;
import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.SuperBuilder;

import java.util.List;


@MappedSuperclass
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public abstract class UserEntity {

    @Column(unique = true)
    private String username;
    @Column(unique = true)
    private String email;
    private String password;
    private Boolean enabled;
    @ElementCollection
    private List<UserAuthority> authority;
    private Boolean emailVerified = false;
    private String phoneNumber;
    private Boolean phoneNumberVerified = false;
    private String birthDate;
    private String firstName;
    private String lastName;
    @Enumerated(EnumType.STRING)
    private Gender gender;



    public UserEntity(String username, String password, Boolean enabled, List<UserAuthority> authority) {
        this.username = username;
        this.password = password;
        this.enabled = enabled;
        this.authority = authority;
    }

    public UserEntity(String lastname, String firstName, String birthDate, String phoneNumber) {
        this.lastName = lastname;
        this.firstName = firstName;
        this.birthDate = birthDate;
        this.phoneNumber = phoneNumber;
    }

}
