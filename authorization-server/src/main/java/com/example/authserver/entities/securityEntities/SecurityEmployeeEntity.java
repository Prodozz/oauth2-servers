package com.example.authserver.entities.securityEntities;

import com.example.authserver.entities.CustomerEntity;
import com.example.authserver.entities.EmployeeEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;

@AllArgsConstructor
@Setter
@Getter
@Builder
public class SecurityEmployeeEntity implements UserDetails {

    private EmployeeEntity employee;
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return employee.getAuthority();
    }

    @Override
    public String getPassword() {
        return employee.getPassword();
    }

    @Override
    public String getUsername() {
        return employee.getUsername();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
