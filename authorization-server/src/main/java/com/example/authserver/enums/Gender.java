package com.example.authserver.enums;

public enum Gender {
    MALE,
    FEMALE,
    NON_BINARY;

}
