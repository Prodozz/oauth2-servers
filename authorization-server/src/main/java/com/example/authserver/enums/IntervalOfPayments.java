package com.example.authserver.enums;

public enum IntervalOfPayments {

    BI_WEEKLY,
    MONTHLY,
    QUARTERLY,
    SEMIANUALLY


}
