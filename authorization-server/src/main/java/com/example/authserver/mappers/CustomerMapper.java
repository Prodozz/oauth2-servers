package com.example.authserver.mappers;

import com.example.authserver.DTOs.customerDTOs.CustomerUserPropertiesDTO;
import com.example.authserver.DTOs.customerDTOs.FullCustomerDTO;
import com.example.authserver.DTOs.customerDTOs.RegisterCustomerDTO;
import com.example.authserver.entities.CustomerEntity;
import org.mapstruct.*;
import org.mapstruct.factory.Mappers;

import java.util.UUID;

@Mapper(componentModel = "spring", uses = {UUID.class, UserMapper.class}, unmappedTargetPolicy = ReportingPolicy.WARN)
public interface CustomerMapper {

    CustomerMapper CUSTOMER_MAPPER = Mappers.getMapper(CustomerMapper.class);

    FullCustomerDTO toDto(CustomerEntity customerEntity);

    @Mapping(target = "customerId", ignore = true)
    CustomerEntity createCustomerEntity(RegisterCustomerDTO registerCustomerDTO);


    @Mapping(target = "customerId", ignore = true)
    void updateEntity(CustomerUserPropertiesDTO customerUserPropertiesDTO, @MappingTarget CustomerEntity customerEntity);

    @Mapping(target = "customerId", ignore = true)
    @Mapping(target = "password", ignore = true)
    void updateEntity(RegisterCustomerDTO registerCustomerDTO, @MappingTarget CustomerEntity customerEntity);

    CustomerUserPropertiesDTO EntityToCredentialsDTO(CustomerEntity customerEntity);



}

