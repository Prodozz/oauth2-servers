package com.example.authserver.mappers;

import com.example.authserver.DTOs.employeeDTOs.EmployeeUserPropertiesDTO;
import com.example.authserver.DTOs.employeeDTOs.FullEmployeeDTO;
import com.example.authserver.DTOs.employeeDTOs.RegisterEmployeeDTO;
import com.example.authserver.entities.EmployeeEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import java.util.UUID;


@Mapper(componentModel = "spring", uses = {UUID.class, UserMapper.class}, unmappedTargetPolicy = ReportingPolicy.WARN)
public interface EmployeeMapper {

    EmployeeMapper EMPLOYEE_MAPPER = Mappers.getMapper(EmployeeMapper.class);

    FullEmployeeDTO toDto(EmployeeEntity employeeEntity);

    EmployeeUserPropertiesDTO EntityToCredentialsDTO(EmployeeEntity employeeEntity);


    @Mapping(target = "employeeId", ignore = true)
    void updateEntity(EmployeeUserPropertiesDTO employeeUserPropertiesDTO, @MappingTarget EmployeeEntity employeeEntity);

    @Mapping(target = "employeeId", ignore = true)
    void updateEntity(RegisterEmployeeDTO registerEmployeeDTO, @MappingTarget EmployeeEntity employeeEntity);



}
