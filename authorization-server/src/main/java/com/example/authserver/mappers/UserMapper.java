package com.example.authserver.mappers;

import com.example.authserver.DTOs.customerDTOs.RegisterCustomerDTO;
import com.example.authserver.DTOs.employeeDTOs.RegisterEmployeeDTO;
import com.example.authserver.DTOs.userDTOs.UserDTO;
import com.example.authserver.config.UserAuthority;
import com.example.authserver.entities.CustomerEntity;
import com.example.authserver.entities.EmployeeEntity;
import com.example.authserver.entities.UserEntity;
import org.mapstruct.*;
import org.mapstruct.factory.Mappers;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.List;
import java.util.stream.Collectors;

import static com.example.authserver.config.UserAuthority.fromStringToAuthority;

@Mapper(componentModel = "spring", uses = {SimpleGrantedAuthority.class}, unmappedTargetPolicy = ReportingPolicy.WARN)
public interface UserMapper {

    UserMapper USER_MAPPER = Mappers.getMapper(UserMapper.class);


    @Mapping(source = "authority", target = "authority", qualifiedByName = "fromAuthorityToString")
    UserDTO toDto(UserEntity userEntity);

    @ObjectFactory
    @Mapping(source = "authority", target = "authority", qualifiedByName = "fromStringToAuthority")
    UserEntity createUserEntity(UserDTO userDTO);



    @Mapping(source = "authority", target = "authority", qualifiedByName = "fromStringToAuthority")
    UserEntity toEntity(UserDTO userDTO);


    @Named("fromStringToAuthority")
    default List<UserAuthority> fromStringToAuth(List<String> authority){
        return authority.stream().map(UserAuthority::fromStringToAuthority).collect(Collectors.toList());
    }
    @Named("fromAuthorityToString")
    default List<String> fromAuthorityToString(List<UserAuthority> authority){
        return authority.stream().map(UserAuthority::getAuthority).collect(Collectors.toList());
    }

}
