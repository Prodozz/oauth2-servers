package com.example.authserver.repositories;

import com.example.authserver.DTOs.customerDTOs.RegisterCustomerDTO;
import com.example.authserver.entities.CustomerEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface CustomerCRUDRepository extends CrudRepository<CustomerEntity, UUID> {

    @Query("select c from CustomerEntity c where c.customerId = :uuid")
    Optional<CustomerEntity> findByCustomerId(@Param("uuid") UUID uuid);

    @Query("select NEW CustomerEntity(c.customerId, c.lastName, c.firstName, c.birthDate, c.phoneNumber )" +
            " from CustomerEntity c where c.customerId = :customerId")
    Optional<CustomerEntity> getCustomerPropertiesByCustomerId(@Param("customerId") UUID customerId);


    Optional<CustomerEntity> findByUsername(String username);
    void deleteByUsername(String username);


}
