package com.example.authserver.repositories;

import com.example.authserver.DTOs.employeeDTOs.RegisterEmployeeDTO;
import com.example.authserver.entities.EmployeeEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface EmployeeCRUDRepository extends CrudRepository<EmployeeEntity, UUID> {

    @Query("select e from EmployeeEntity e where e.employeeId = :uuid")
    Optional<EmployeeEntity> findByEmployeeId(@Param("uuid") UUID uuid);


    @Query("select NEW EmployeeEntity (e.employeeId, e.lastName, e.firstName," +
            " e.birthDate, e.phoneNumber, e.grossSalary, e.employmentType, e.position )" +
            " from EmployeeEntity e where e.employeeId = :employeeId")
    Optional<EmployeeEntity> getEmployeePropertiesByUserId(@Param("employeeId") UUID employeeId);

    Optional<EmployeeEntity> findByUsername(String username);

}
