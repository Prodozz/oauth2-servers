package com.example.authserver.security;

import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.expression.SecurityExpressionRoot;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.server.resource.authentication.BearerTokenAuthentication;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

@Component
@RequiredArgsConstructor
public class CustomMethodSecurity {
    private final UserDetailsManager userDetailsManager;

    public Collection<? extends GrantedAuthority> getAuthorities(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetails user = userDetailsManager.loadUserByUsername(authentication.getName());
        return user.getAuthorities();

    }

    public Boolean hasAnyRoleAuthority(String... roles) {
        Collection<? extends GrantedAuthority> authorities = getAuthorities();
        for (GrantedAuthority authority : authorities) {
            for (String role : roles) {
                if (authority.getAuthority().equals("ROLE_" + role)){
                    return true;
                }
            }
        }
        return false;
    }

    public Boolean isAdmin(){
        return hasAnyRoleAuthority("ADMIN");
    }

    public Boolean isEmployee(){
        return hasAnyRoleAuthority("EMPLOYEE");
    }

    public Boolean isCustomer(){
        return hasAnyRoleAuthority("CUSTOMER");
    }

}
