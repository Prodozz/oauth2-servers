package com.example.authserver.security.config;

import com.example.authserver.security.service.OidcUserInfoService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.oauth2.core.OAuth2AccessToken;
import org.springframework.security.oauth2.core.oidc.OidcUserInfo;
import org.springframework.security.oauth2.core.oidc.endpoint.OidcParameterNames;
import org.springframework.security.oauth2.server.authorization.OAuth2TokenType;
import org.springframework.security.oauth2.server.authorization.token.JwtEncodingContext;
import org.springframework.security.oauth2.server.authorization.token.OAuth2TokenCustomizer;

import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Configuration
public class JwtTokenCustomizerConfig {


    @Bean
    public OAuth2TokenCustomizer<JwtEncodingContext> tokenCustomizer(OidcUserInfoService userInfoService) {
        return (jwtEncodingContext) -> {
            Authentication authentication = jwtEncodingContext.getPrincipal();

            if (jwtEncodingContext.getTokenType().equals(OAuth2TokenType.ACCESS_TOKEN)
                    && authentication instanceof UsernamePasswordAuthenticationToken) {


                Set<String> authorities = authentication.getAuthorities().stream()
                        .map(GrantedAuthority::getAuthority).collect(Collectors.toSet());

//                User user = (User) authentication.getPrincipal();
                jwtEncodingContext.getClaims().claim("authorities", authorities);

            }


            if (jwtEncodingContext.getTokenType().getValue().equals(OidcParameterNames.ID_TOKEN)) {
                OidcUserInfo userInfo = userInfoService.mapUserToOidcUserInfo(jwtEncodingContext.getPrincipal().getName());

                jwtEncodingContext.getClaims().claims(claims ->
                        claims.putAll(userInfo.getClaims()));
            }

        };
    }


}

//                  FOR ACCESSTOKEN CUSTOMIZATION
