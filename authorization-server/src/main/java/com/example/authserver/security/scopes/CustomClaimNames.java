package com.example.authserver.security.scopes;

public final class CustomClaimNames {

    public static final String PAYMENT_METHOD = "paymentMethod";
    public static final String STATUS = "status";
    public static final String HAS_CREDIT_WORTHINESS = "hasCreditWorthiness";

}
