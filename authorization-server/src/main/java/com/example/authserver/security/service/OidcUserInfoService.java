package com.example.authserver.security.service;

import java.util.NoSuchElementException;

import com.example.authserver.config.UserAuthority;
import com.example.authserver.entities.CustomerEntity;
import com.example.authserver.entities.EmployeeEntity;
import com.example.authserver.repositories.CustomerCRUDRepository;
import com.example.authserver.repositories.EmployeeCRUDRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.oauth2.core.oidc.OidcUserInfo;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class OidcUserInfoService {
    private final EmployeeCRUDRepository employeeCRUDRepository;
    private final CustomerCRUDRepository customerCRUDRepository;
    private final UserDetailsManager userDetailsManager;


    public OidcUserInfo mapUserToOidcUserInfo(String username) {
        UserDetails userDetails = userDetailsManager.loadUserByUsername(username);
        OidcUserInfo.Builder userInfo = OidcUserInfo.builder()
                .subject(username)
                .picture("not yet configured")
                .locale("de-AT")
                .profile("https://example.com/")
                .claim("role", userDetails.getAuthorities().toString());

        if (userDetails.getAuthorities().contains(UserAuthority.EMPLOYEE)) {
            EmployeeEntity employee = employeeCRUDRepository.findByUsername(username)
                    .orElseThrow(() -> new NoSuchElementException("Error!"));


            userInfo.email(employee.getEmail())
                    .emailVerified(employee.getEmailVerified())
                    .name(employee.getFirstName() + " " + employee.getLastName())
                    .givenName(employee.getFirstName())
                    .familyName(employee.getLastName())
                    .gender(employee.getGender() != null ? employee.getGender().name() : null)
                    .birthdate(employee.getBirthDate())
                    .phoneNumber(employee.getPhoneNumber())
                    .phoneNumberVerified(employee.getPhoneNumberVerified())
                    .claim("grossSalary", employee.getGrossSalary())
                    .claim("employmentType", employee.getEmploymentType().name())
                    .claim("position", employee.getPosition().name());

        } else if (userDetails.getAuthorities().contains(UserAuthority.CUSTOMER)) {
            CustomerEntity customer = customerCRUDRepository.findByUsername(username)
                    .orElseThrow(() -> new NoSuchElementException("Error!"));


            userInfo.email(customer.getEmail())
                    .emailVerified(customer.getEmailVerified())
                    .name(customer.getFirstName() + " " + customer.getLastName())
                    .givenName(customer.getFirstName())
                    .familyName(customer.getLastName())
                    .gender(customer.getGender() != null ? customer.getGender().name() : null)
                    .birthdate(customer.getBirthDate())
                    .phoneNumber(customer.getPhoneNumber())
                    .phoneNumberVerified(customer.getPhoneNumberVerified())
                    .claim("paymentMethod", customer.getPaymentMethod() != null ? customer.getPaymentMethod().name() : null)
                    .claim("status", customer.getStatus() != null ? customer.getStatus().name() : null)
                    .claim("hasCreditWorthiness", customer.getHasCreditWorthiness() != null ? customer.getHasCreditWorthiness() : null);
        }

        return userInfo.build();
    }
}

