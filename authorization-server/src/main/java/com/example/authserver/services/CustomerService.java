package com.example.authserver.services;

import com.example.authserver.DTOs.customerDTOs.*;
import com.example.authserver.entities.*;
import com.example.authserver.enums.PaymentMethod;
import com.example.authserver.enums.Status;
import com.example.authserver.mappers.CustomerMapper;
import com.example.authserver.mappers.UserMapper;
import com.example.authserver.repositories.*;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CustomerService {

    private final CustomerCRUDRepository customerCRUDRepository;
    private final CustomerMapper customerMapper;



    public List<FullCustomerDTO> getAllCustomers() {

        List<CustomerEntity> customerEntities = (List<CustomerEntity>) customerCRUDRepository.findAll();
        return customerEntities.stream().map(customerMapper::toDto).collect(Collectors.toList());
    }


    public CustomerUserPropertiesDTO getCustomerPropertiesByDTO(RegisterCustomerDTO registerCustomerDTO) throws NoSuchElementException {

        CustomerEntity customerEntity = customerCRUDRepository.findByUsername(registerCustomerDTO.getUsername())
                .orElseThrow(() -> new NoSuchElementException("Error!"));

        return customerMapper.EntityToCredentialsDTO(customerEntity);
    }

    public CustomerUserPropertiesDTO getCustomerPropertiesByUsername() throws NoSuchElementException {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        CustomerEntity customerEntity = customerCRUDRepository.findByUsername(authentication.getName())
                .orElseThrow(() -> new NoSuchElementException("Error!"));

        return customerMapper.EntityToCredentialsDTO(customerEntity);
    }


    public FullCustomerDTO getCustomer(String customerId) throws NoSuchElementException {
        CustomerEntity customerEntity = customerCRUDRepository.findByCustomerId(UUID.fromString(customerId))
                .orElseThrow(() -> new NoSuchElementException("Error!"));

        return customerMapper.toDto(customerEntity);
    }

    public FullCustomerDTO getCustomerByUsername() {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();


        CustomerEntity customer = customerCRUDRepository.findByUsername(authentication.getName())
                .orElseThrow(() -> new NoSuchElementException("Error!"));


        return customerMapper.toDto(customer);
    }


    public void createCustomer(RegisterCustomerDTO registerCustomerDTO) {

        CustomerEntity customerEntity = new CustomerEntity();
        customerMapper.updateEntity(registerCustomerDTO, customerEntity);

        customerEntity.setUsername(registerCustomerDTO.getUsername());
        customerEntity.setStatus(Status.NOT_VERIFIED);
        customerCRUDRepository.save(customerEntity);

    }

    public void deleteCustomerById(String customerId) {
        customerCRUDRepository.deleteById(UUID.fromString(customerId));
    }

    public void deleteCustomerByUsername(String username) {
        customerCRUDRepository.deleteByUsername(username);
    }


    public void updateCustomerPropertiesByUsername(CustomerUserPropertiesDTO customerUserPropertiesDTO) {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();


        CustomerEntity customer = customerCRUDRepository
                .findByUsername(authentication.getName())
                .orElseThrow(() -> new NoSuchElementException("Error!"));

        customerMapper.updateEntity(customerUserPropertiesDTO, customer);
        customerCRUDRepository.save(customer);
    }

    public void setPayment(SetPaymentDTO paymentMethod) {

        CustomerEntity customer = customerCRUDRepository.findById(UUID.fromString(paymentMethod.getCustomerId()))
                .orElseThrow(() -> new NoSuchElementException("Error!"));

        customer.setPaymentMethod(PaymentMethod.getPaymentMethodFromString(paymentMethod.getPaymentMethod()));

        customerCRUDRepository.save(customer);
    }

    public void setStatus(SetStatusDTO setStatusDTO){

        CustomerEntity customer = customerCRUDRepository.findByCustomerId(UUID.fromString(setStatusDTO.getCustomerId()))
                .orElseThrow(() -> new NoSuchElementException("Error"));

        customer.setStatus(Status.stringToStatus(setStatusDTO.getStatus()));

        customerCRUDRepository.save(customer);

    }

}
