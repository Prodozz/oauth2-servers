package com.example.authserver.services;

import com.example.authserver.DTOs.customerDTOs.RegisterCustomerDTO;
import com.example.authserver.entities.CustomerEntity;
import com.example.authserver.entities.securityEntities.SecurityCustomerEntity;
import com.example.authserver.mappers.CustomerMapper;
import com.example.authserver.repositories.CustomerCRUDRepository;
import lombok.AllArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.example.authserver.config.UserAuthority.CUSTOMER;

@Service
@AllArgsConstructor
public class CustomerUserService {

    private CustomerService customerService;
    private UserDetailsManager userDetailsManager;
    private PasswordEncoder passwordEncoder;
    private CustomerMapper  customerMapper;

    public void createCustomerUser(RegisterCustomerDTO registerCustomerDTO) {

        CustomerEntity customerEntity = new CustomerEntity();
        customerMapper.updateEntity(registerCustomerDTO, customerEntity);

        customerEntity.setPassword(passwordEncoder.encode(registerCustomerDTO.getPassword()));
        customerEntity.setAuthority(List.of(CUSTOMER));

        SecurityCustomerEntity securityCustomerEntity = SecurityCustomerEntity.builder()
                .customer(customerEntity)
                .build();


        userDetailsManager.createUser(securityCustomerEntity);
        customerService.createCustomer(registerCustomerDTO);

    }


    public void deleteCustomerByUsername(String username) {
        customerService.deleteCustomerByUsername(username);
        userDetailsManager.deleteUser(username);

    }

    public void deleteCustomerById(String customerId) {
        customerService.deleteCustomerById(customerId);
        userDetailsManager.deleteUser(customerId);

    }


}
