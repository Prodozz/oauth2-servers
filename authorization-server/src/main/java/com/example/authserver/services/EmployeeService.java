package com.example.authserver.services;

import com.example.authserver.DTOs.employeeDTOs.EmployeeUserPropertiesDTO;
import com.example.authserver.DTOs.employeeDTOs.FullEmployeeDTO;
import com.example.authserver.DTOs.employeeDTOs.RegisterEmployeeDTO;
import com.example.authserver.entities.*;
import com.example.authserver.mappers.EmployeeMapper;
import com.example.authserver.repositories.*;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class EmployeeService {

    EmployeeCRUDRepository employeeCRUDRepository;
    EmployeeMapper employeeMapper;

    public List<FullEmployeeDTO> getAllEmployees() {

        List<EmployeeEntity> employeeEntities = (List<EmployeeEntity>) employeeCRUDRepository.findAll();
        return employeeEntities.stream().map(employeeMapper::toDto).collect(Collectors.toList());

    }

    public EmployeeUserPropertiesDTO getEmployeePropertiesByDTO(String username) throws NoSuchElementException {

        EmployeeEntity employeeEntity = employeeCRUDRepository.findByUsername(username)
                .orElseThrow(() -> new NoSuchElementException("Error!"));

        return employeeMapper.EntityToCredentialsDTO(employeeEntity);
    }

    public EmployeeUserPropertiesDTO getEmployeePropertiesById(String employeeId) throws NoSuchElementException {

        EmployeeEntity employeeEntity = employeeCRUDRepository.getEmployeePropertiesByUserId(UUID.fromString(employeeId))
                .orElseThrow(() -> new NoSuchElementException("Error!"));

        return employeeMapper.EntityToCredentialsDTO(employeeEntity);
    }


    public FullEmployeeDTO getEmployee(String employeeId) {

        EmployeeEntity employeeEntity = employeeCRUDRepository.findByEmployeeId(UUID.fromString(employeeId))
                .orElseThrow(() -> new NoSuchElementException("Error!"));

        return employeeMapper.toDto(employeeEntity);

    }


    public void createEmployee(RegisterEmployeeDTO registerEmployeeDTO) {

        EmployeeEntity employeeEntity = new EmployeeEntity();
        employeeMapper.updateEntity(registerEmployeeDTO, employeeEntity);

        employeeEntity.setUsername(registerEmployeeDTO.getUsername());
        employeeCRUDRepository.save(employeeEntity);

    }

    public String deleteEmployee(String employeeId) {

        EmployeeEntity employee = employeeCRUDRepository.findById(UUID.fromString(employeeId))
                .orElseThrow(() -> new NoSuchElementException("Error!"));


        employeeCRUDRepository.deleteById(UUID.fromString(employeeId));

        return employee.getUsername();

    }

    public void updateEmployeeProperties(EmployeeUserPropertiesDTO employeeUserDPropertiesDTO) {

        EmployeeEntity employeeEntity = employeeCRUDRepository
                .findByEmployeeId(UUID.fromString(employeeUserDPropertiesDTO.getEmployeeId()))
                .orElseThrow(() -> new NoSuchElementException("Error!"));

        employeeMapper.updateEntity(employeeUserDPropertiesDTO, employeeEntity);

        employeeCRUDRepository.save(employeeEntity);

    }


}
