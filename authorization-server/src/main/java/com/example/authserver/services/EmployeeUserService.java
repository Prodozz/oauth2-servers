package com.example.authserver.services;

import com.example.authserver.DTOs.employeeDTOs.RegisterEmployeeDTO;
import com.example.authserver.entities.CustomerEntity;
import com.example.authserver.entities.EmployeeEntity;
import com.example.authserver.entities.securityEntities.SecurityEmployeeEntity;
import com.example.authserver.mappers.EmployeeMapper;
import lombok.AllArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.example.authserver.config.UserAuthority.*;

@Service
@AllArgsConstructor
public class EmployeeUserService {

    private EmployeeService employeeService;
    private UserDetailsManager userDetailsManager;
    private PasswordEncoder passwordEncoder;
    private EmployeeMapper employeeMapper;

    public void createEmployeeUser(RegisterEmployeeDTO registerEmployeeDTO) {

        EmployeeEntity employeeEntity = new EmployeeEntity();
        employeeMapper.updateEntity(registerEmployeeDTO, employeeEntity);

        employeeEntity.setPassword(passwordEncoder.encode(registerEmployeeDTO.getPassword()));
        employeeEntity.setAuthority(List.of(EMPLOYEE));


        SecurityEmployeeEntity securityUserEntity = SecurityEmployeeEntity.builder()
                .employee(employeeEntity)
                .build();

        userDetailsManager.createUser(securityUserEntity);
        employeeService.createEmployee(registerEmployeeDTO);

    }


    public void deleteEmployeeUser(String employeeId) {
        String username = employeeService.deleteEmployee(employeeId);
        userDetailsManager.deleteUser(username);

    }


//    public void updateEmployeeUser(CustomerUserPropertiesDTO userCredentialsDTO) {
//
//        UserDetails userDetails = User.builder()
//                .username(userCredentialsDTO.getUsername())
//                .password(passwordEncoder.encode(userCredentialsDTO.getPassword()))
//                .roles(userCredentialsDTO.getUserRole())
//                .build();
//
//        userDetailsManager.updateUser(userDetails);
//        employeeService.updateEmployee(userCredentialsDTO);
//
//    }
}
