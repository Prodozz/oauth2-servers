package com.example.authserver.services;

import com.example.authserver.DTOs.userDTOs.UserDetailsDTO;
import com.example.authserver.config.UserAuthority;
import com.example.authserver.entities.CustomerEntity;
import com.example.authserver.entities.EmployeeEntity;
import com.example.authserver.entities.securityEntities.SecurityCustomerEntity;
import com.example.authserver.entities.UserEntity;
import com.example.authserver.entities.securityEntities.SecurityEmployeeEntity;
import lombok.AllArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

import static com.example.authserver.config.UserAuthority.CUSTOMER;
import static com.example.authserver.config.UserAuthority.EMPLOYEE;

@Service
@AllArgsConstructor
public class UserEntityService {

    private final PasswordEncoder passwordEncoder;

    public SecurityCustomerEntity createSecurityCustomer(UserDetailsDTO userDetailsDTO){
        return SecurityCustomerEntity.builder()
                .customer(
                        new CustomerEntity(
                                userDetailsDTO.getUsername(),
                                passwordEncoder.encode(userDetailsDTO.getPassword()),
                                true,
                                List.of(CUSTOMER)
                        )
                ).build();

    }

    public SecurityEmployeeEntity createSecurityEmployee(UserDetailsDTO userDetailsDTO){
        return SecurityEmployeeEntity.builder()
                .employee(
                        new EmployeeEntity(
                                userDetailsDTO.getUsername(),
                                passwordEncoder.encode(userDetailsDTO.getPassword()),
                                true,
                                List.of(EMPLOYEE)
                        )
                ).build();
    }


}
