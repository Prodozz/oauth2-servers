    package com.example.resourceserver.address;

    import org.springframework.data.repository.CrudRepository;
    import org.springframework.stereotype.Repository;

    import java.util.List;
    import java.util.Optional;
    import java.util.UUID;

    @Repository
    public interface AddressCRUDRepository extends CrudRepository<AddressEntity, UUID> {

        Optional<AddressEntity> findByAddressId(UUID addressId);
        void deleteByAddressId(UUID addressId);
        List<AddressEntity> findAllByUsername(String username);

        AddressEntity findByUsername(String username);
        void deleteByUsername (String username);


    }
