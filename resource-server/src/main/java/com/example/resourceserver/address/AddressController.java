package com.example.resourceserver.address;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping(path = "/api/address")
@AllArgsConstructor
public class AddressController {

    final AddressService addressService;
    final AddressCRUDRepository addressCRUDRepository;

    @GetMapping("/getAll")
    @PreAuthorize("@customMethodSecurity.isAdmin()")
    public ResponseEntity<?> getAllAddresses() {
        try {
            return ResponseEntity.ok(addressService.getAllAddresses());
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/{addressId}")
    @PreAuthorize("@customMethodSecurity.hasAnyRoleAuthority('ADMIN', 'CUSTOMER')")
    public ResponseEntity<?> getAddressById(@PathVariable String addressId) {
        try {
            return ResponseEntity.ok(addressService.getAddressById(addressId));
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }


    @PutMapping("/{addressId}")
    public ResponseEntity<?> updateAddress(@RequestBody AddressDTO addressDTO, @PathVariable String addressId) {
        try {
            addressService.updateAddressById(addressDTO, addressId);
            return ResponseEntity.ok(addressService.getAllAddresses());
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }


    @DeleteMapping("/{addressId}")
    @PreAuthorize("@customMethodSecurity.isAdmin()")
    public ResponseEntity<?> deleteAddress(@PathVariable String addressId) {
        try {
            addressService.deleteAddress(addressId);
            return ResponseEntity.ok(addressService.getAllAddresses());
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }


    /*


    User Requests


     */

    @GetMapping
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<?> getAddressByUsername() {
        try {
            String username = SecurityContextHolder.getContext().getAuthentication().getName();
            return ResponseEntity.ok(addressService.getAddressByUsername(username));
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping
    @PreAuthorize("@customMethodSecurity.hasAnyRoleAuthority('ADMIN','EMPLOYEE')")
    public ResponseEntity<?> createAddress(@RequestBody AddressDTO addressDTO) {
        try {
            addressService.createAddress(addressDTO);
            return ResponseEntity.ok(addressService.getAllAddresses());
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }


    @PutMapping
    public ResponseEntity<?> updateAddress(@RequestBody AddressDTO addressDTO) {
        try {
            addressService.updateAddressByUsername(addressDTO);
            return ResponseEntity.ok(addressService.getAllAddresses());
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }


    @DeleteMapping
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<?> deleteAddress() {
        try {
            addressService.deleteAddressByUsername();
            return ResponseEntity.ok(addressService.getAllAddresses());
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }


}
















