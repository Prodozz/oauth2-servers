package com.example.resourceserver.address;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@Data
public class AddressDTO {

    private String state;
    private String district;
    private String streetName;
    private int streetNumber;
    private int houseNumber;
    private int door;
}
