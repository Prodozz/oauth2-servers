package com.example.resourceserver.address;

import jakarta.persistence.*;
import lombok.*;

import java.util.UUID;

@Entity
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class AddressEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private UUID addressId;
    private String state;
    private String district;
    private String streetName;
    private int streetNumber;
    private int houseNumber;
    private int door;
    private String username;


}
