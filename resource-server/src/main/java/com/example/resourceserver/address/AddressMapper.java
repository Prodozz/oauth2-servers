package com.example.resourceserver.address;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

import java.util.UUID;


@Mapper(componentModel = "spring", uses = {UUID.class})
public interface AddressMapper {

    AddressMapper ADDRESS_MAPPER = Mappers.getMapper(AddressMapper.class);


    AddressDTO toDto(AddressEntity addressEntity);
    FullAddressDTO toFullDto(AddressEntity addressEntity);

    @Mapping(target = "addressId", ignore = true)
    AddressEntity toEntity(AddressDTO addressDTO);


    @Mapping(target = "addressId", ignore = true)
    void updateEntity(AddressDTO addressDTO, @MappingTarget AddressEntity addressEntity);




}
