package com.example.resourceserver.address;

import lombok.AllArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class AddressService {

    AddressMapper addressMapper;
    AddressCRUDRepository addressCRUDRepository;

    public List<FullAddressDTO> getAllAddresses() {

        List<AddressEntity> addressEntities = (List<AddressEntity>) addressCRUDRepository.findAll();
        return addressEntities.stream().map(addressMapper::toFullDto).collect(Collectors.toList());

    }

    public AddressDTO getAddressById(String addressId) {

        AddressEntity addressEntity = addressCRUDRepository.findByAddressId(UUID.fromString(addressId))
                .orElseThrow(() -> new NoSuchElementException("Error!"));
        return addressMapper.toDto(addressEntity);

    }

    public List<AddressDTO> getAddressByUsername(String username) {

        List<AddressEntity> addressEntities = addressCRUDRepository.findAllByUsername(username);
        return addressEntities.stream().map(addressMapper::toDto).collect(Collectors.toList());

    }

    public void createAddress(AddressDTO addressDTO) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        AddressEntity addressEntity = addressMapper.toEntity(addressDTO);
        addressEntity.setUsername(authentication.getName());

        addressCRUDRepository.save(addressEntity);

    }

    public void deleteAddress(String addressId) {

        addressCRUDRepository.deleteByAddressId(UUID.fromString(addressId));

    }

    public void deleteAddressByUsername() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        addressCRUDRepository.deleteByUsername(authentication.getName() );

    }

    public void updateAddressById(AddressDTO addressDTO, String addressId) {

        AddressEntity addressEntity = addressCRUDRepository.findByAddressId(UUID.fromString(addressId))
                .orElseThrow(() -> new NoSuchElementException("Error!"));
        addressMapper.updateEntity(addressDTO, addressEntity);


        addressCRUDRepository.save(addressEntity);

    }

    public void updateAddressByUsername(AddressDTO addressDTO) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        AddressEntity addressEntity = addressCRUDRepository.findByUsername(authentication.getName());
        addressMapper.updateEntity(addressDTO, addressEntity);


        addressCRUDRepository.save(addressEntity);

    }
}
