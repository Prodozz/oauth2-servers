package com.example.resourceserver.address;

import lombok.Data;

@Data
public class FullAddressDTO {
    private String addressId;
    private String state;
    private String district;
    private String streetName;
    private int streetNumber;
    private int houseNumber;
    private int door;
    private String username;
}
