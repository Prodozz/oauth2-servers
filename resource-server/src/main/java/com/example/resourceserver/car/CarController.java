package com.example.resourceserver.car;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@CrossOrigin
@RequestMapping(path = "/api/car")
@AllArgsConstructor
public class CarController {

    final CarService carService;
    final CarCRUDRepository carCRUDRepository;

    @GetMapping("/getAll")
    @PreAuthorize("@customMethodSecurity.isAdmin()")
    public ResponseEntity<?> getAllCars() {
        try {
            return ResponseEntity.ok(carService.getAllCars());
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/carList")
    @PreAuthorize("@customMethodSecurity.isAdmin()")
    public ResponseEntity<?> getCarsInList(@RequestBody Map<String, String> CarIdList) {
        try {
            return ResponseEntity.ok(carService.getCarsInList(CarIdList));
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/{carId}")
    @PreAuthorize("@customMethodSecurity.hasAnyRoleAuthority('ADMIN','EMPLOYEE')")
    public ResponseEntity<?> getCarById(@PathVariable String carId) {
        try {
            return ResponseEntity.ok(carService.getCarById(carId));
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/update/{carId}")
    @PreAuthorize("@customMethodSecurity.hasAnyRoleAuthority('ADMIN', 'EMPLOYEE')")
    public ResponseEntity<?> updateCarById(@RequestBody CarDTO carDTO) {
        try {
            carService.updateCarById(carDTO);
            return ResponseEntity.ok(carService.getAllCars());
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }


    @DeleteMapping("/{carId}")
    @PreAuthorize("@customMethodSecurity.isAdmin()")
    public ResponseEntity<?> deleteCar(@PathVariable String carId) {
        try {
            carService.deleteCarById(carId);
            return ResponseEntity.ok(carService.getAllCars());
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }


        /*


    User Requests


     */


    @GetMapping
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<?> getCarByUsername() {
        try {
            return ResponseEntity.ok(carService.getCarByUsername());
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping
    @PostAuthorize("isAuthenticated()")
    public ResponseEntity<?> createCar(@RequestBody CreateCarDTO createCarDTO) {
        try {
            carService.createCar(createCarDTO);
            return ResponseEntity.ok(carService.getAllCars());
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<?> updateCarByUsername(@RequestBody CarDTO carDTO) {
        try {
            carService.updateCarByUsername(carDTO);
            return ResponseEntity.ok(carService.getAllCars());
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<?> deleteCarByUsername(@RequestBody Map<String, String> carId) {
        try {
            carService.deleteCarByUsername(carId.get("carId"));
            return ResponseEntity.ok(carService.getAllCars());
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }




}
















