package com.example.resourceserver.car;

import com.example.resourceserver.contract.ContractEntity;
import com.example.resourceserver.enums.enums.Brand;
import com.example.resourceserver.enums.enums.FuelType;
import com.example.resourceserver.enums.enums.ModelName;
import com.example.resourceserver.motorcarAccident.MotorcarAccidentEntity;
import com.example.resourceserver.rating.RatingEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "CarEntity")

public class CarEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private UUID carId;
    @Enumerated(EnumType.STRING)
    private Brand brand;
    @Enumerated(EnumType.STRING)
    private ModelName modelName;
    private Long mileage;
    @Enumerated(EnumType.STRING)
    private FuelType fuelType;
    private String finNumber;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private String whenBought;
    private String buildDate;
    private int kw;
    private String contractTerm;
    private Double monthlyRate;
    @ElementCollection
    private List<String> carProperties;
    private String username;

    @OneToMany(mappedBy = "car")
    private List<ContractEntity> contractEntities = new ArrayList<>();
    @OneToMany(mappedBy = "car")
    private List<RatingEntity> ratingEntities = new ArrayList<>();

    @OneToOne(mappedBy = "car")
    private MotorcarAccidentEntity motorcarAccidentEntity;




}
