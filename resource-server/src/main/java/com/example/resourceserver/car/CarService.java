package com.example.resourceserver.car;

import com.example.resourceserver.contract.ContractCRUDRepository;
import com.example.resourceserver.contract.ContractEntity;
import com.example.resourceserver.rating.RatingCRUDRepository;
import com.example.resourceserver.rating.RatingEntity;
import lombok.AllArgsConstructor;
import org.springframework.expression.ExpressionException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class CarService {

    CarCRUDRepository carCRUDRepository;
    ContractCRUDRepository contractCRUDRepository;
    RatingCRUDRepository ratingCRUDRepository;
    CarMapper carMapper;

    public List<CarDTO> getAllCars() {

        List<CarEntity> carEntities = (List<CarEntity>) carCRUDRepository.findAll();
        return carEntities.stream().map(carEntity -> carMapper.toDto(carEntity)).collect(Collectors.toList());

    }

    public List<CarDTO> getCarsInList(Map<String, String> carList){
        List<CarEntity> allById = (List<CarEntity>) carCRUDRepository.findAllById(carList.values().stream()
                .map(UUID::fromString).collect(Collectors.toList()));

        return allById.stream().map(car -> carMapper.toDto(car)).collect(Collectors.toList());

    }


    public CarDTO getCarById(String carId) {

        CarEntity carEntity = carCRUDRepository
                .findByCarId(UUID.fromString(carId))
                .orElseThrow(() -> new NoSuchElementException("Error!"));

        return carMapper.toDto(carEntity);
    }

    public List<CarDTO> getCarByUsername() {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();

        List<CarEntity> carEntities = carCRUDRepository.findAllByUsername(username);
        return carEntities.stream().map(carEntity -> carMapper.toDto(carEntity)).collect(Collectors.toList());
    }


    public void createCar(CreateCarDTO createCarDTO) throws Exception {
//        if (createCarDTO.getContractEntity() == null) {
//            throw new Exception("Kein Auto ohne Vertrag!");
//        }

        CarEntity carEntity = carMapper.toEntity(createCarDTO);

            List<ContractEntity> contractEntities = carEntity.getContractEntities();
        if (createCarDTO.getContractEntity() != null) {
            contractEntities.add(
                    contractCRUDRepository.findById(UUID.fromString(createCarDTO.getContractEntity()))
                            .orElseThrow(() -> new ExpressionException("Error!"))
            );
        }

        carEntity.setContractEntities(contractEntities);

        carCRUDRepository.save(carEntity);

    }

    public void deleteCarById(String carId) {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        CarEntity carEntity = carCRUDRepository.findAllByUsername(authentication.getName())
                .stream().filter(
                        car -> car.getCarId().toString().equals(carId)
                )
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("Error!"));

        for (ContractEntity contractEntity : carEntity.getContractEntities()) {
            contractEntity.setCar(null);
            contractCRUDRepository.save(contractEntity);
        }
        for (RatingEntity ratingEntity : carEntity.getRatingEntities()) {
            ratingEntity.setRating(null);
            ratingCRUDRepository.save(ratingEntity);
        }

        carCRUDRepository.deleteByCarId(UUID.fromString(carId));

    }

    public void deleteCarByUsername(String carId) {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        List<CarEntity> carEntityList = carCRUDRepository.findAllByUsername(authentication.getName());
        CarEntity car = carEntityList.stream().filter(
                        carEntity -> carEntity.getCarId().toString().equals(carId)
                ).findFirst()
                .orElseThrow(() -> new NoSuchElementException("Error!"));


        for (ContractEntity contractEntity : car.getContractEntities()) {
            contractEntity.setCar(null);
            contractCRUDRepository.save(contractEntity);
        }
        for (RatingEntity ratingEntity : car.getRatingEntities()) {
            ratingEntity.setRating(null);
            ratingCRUDRepository.save(ratingEntity);
        }

        carCRUDRepository.deleteByCarId(UUID.fromString(carId));

    }

    public void updateCarByUsername(CarDTO carDTO) {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        List<CarEntity> carEntities = carCRUDRepository.findAllByUsername(authentication.getName());
        CarEntity car = carEntities.stream().filter(
                        carEntity -> carEntity.getCarId().toString().equals(carDTO.getCarId())
                ).findFirst()
                .orElseThrow(() -> new NoSuchElementException("Error!"));


        carMapper.updateEntity(carDTO, car);

        List<ContractEntity> contractEntities = null;
        List<RatingEntity> ratingEntities = null;

        if (carDTO.getContractEntities() != null && !carDTO.getContractEntities().isEmpty()) {
            contractEntities = carDTO.getContractEntities().stream()
                    .map(
                            s -> contractCRUDRepository.findByContractId(UUID.fromString(s))
                                    .orElseThrow(() -> new NoSuchElementException("Error!"))
                    ).collect(Collectors.toList());
        }
        if (carDTO.getRatingEntities() != null && !carDTO.getRatingEntities().isEmpty()) {
            ratingEntities = carDTO.getRatingEntities().stream()
                    .map(
                            s -> ratingCRUDRepository.findById(UUID.fromString(s))
                                    .orElseThrow(() -> new NoSuchElementException("Error!"))
                    ).collect(Collectors.toList());
        }

        car.setContractEntities(contractEntities);
        car.setRatingEntities(ratingEntities);

        carCRUDRepository.save(car);
    }


    public void updateCarById(CarDTO carDTO) {

        CarEntity carEntity = carCRUDRepository.findByCarId(UUID.fromString(carDTO.getCarId()))
                .orElseThrow(() -> new NoSuchElementException("Error!"));
        carMapper.updateEntity(carDTO, carEntity);

        List<ContractEntity> contractEntities = null;
        List<RatingEntity> ratingEntities = null;

        if (carDTO.getContractEntities() != null && !carDTO.getContractEntities().isEmpty()) {
            contractEntities = carDTO.getContractEntities().stream()
                    .map(
                            s -> contractCRUDRepository.findByContractId(UUID.fromString(s))
                                    .orElseThrow(() -> new NoSuchElementException("Error!"))
                    ).collect(Collectors.toList());
        }
        if (carDTO.getRatingEntities() != null && !carDTO.getRatingEntities().isEmpty()) {
            ratingEntities = carDTO.getRatingEntities().stream()
                    .map(
                            s -> ratingCRUDRepository.findById(UUID.fromString(s))
                                    .orElseThrow(() -> new NoSuchElementException("Error!"))
                    ).collect(Collectors.toList());
        }

        carEntity.setContractEntities(contractEntities);
        carEntity.setRatingEntities(ratingEntities);

        carCRUDRepository.save(carEntity);
    }
}
