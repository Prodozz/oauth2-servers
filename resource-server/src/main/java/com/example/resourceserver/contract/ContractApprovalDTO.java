package com.example.resourceserver.contract;

import lombok.Data;

@Data
public class ContractApprovalDTO {

    private String contractId;
    private Boolean isActive;
    private String feedbackText;
    private String contractPaymentStatusId;
    private Boolean isApproved;

}
