package com.example.resourceserver.contract;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface ContractCRUDRepository extends CrudRepository<ContractEntity, UUID> {


    Optional<ContractEntity> findByContractId(UUID uuid);
    Optional<ContractEntity> findByEmployeeId(String employeeId);
    List<ContractEntity> findByUsername(String username);
    void deleteByContractId(UUID uuid);

}
