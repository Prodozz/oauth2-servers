package com.example.resourceserver.contract;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping(path = "/api/contract")
@AllArgsConstructor
public class ContractController {

    final ContractService contractService;
    final ContractCRUDRepository contractCRUDRepository;

    @GetMapping("/getAll")
    @PreAuthorize("@customMethodSecurity.isAdmin()")
    public ResponseEntity<?> getAllContracts() {
        try {
            return ResponseEntity.ok(contractService.getAllContracts());
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/auth/{employeeId}")
    @PreAuthorize("@customMethodSecurity.isAdmin()")
    public ResponseEntity<?> getContractByEmployeeId(@PathVariable String employeeId) {
        try {
            return ResponseEntity.ok(contractService.getContractByEmployeeId(employeeId));
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/{contractId}")
    @PreAuthorize("@customMethodSecurity.hasAnyRoleAuthority('ADMIN', 'EMPLOYEE')")
    public ResponseEntity<?> getContractById(@PathVariable String contractId) {
        try {
            return ResponseEntity.ok(contractService.getContractById(contractId));
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<?> contractSuggestion(@RequestBody ContractSuggestionDTO contractDTO) {
        try {
            contractService.suggestContract(contractDTO);
            return ResponseEntity.ok(contractService.getAllContracts());
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/approve")
    @PreAuthorize("@customMethodSecurity.hasAnyRoleAuthority('ADMIN','EMPLOYEE')")
    public ResponseEntity<?> approveContract(@RequestBody ContractApprovalDTO contractDTO) {
        try {

            contractService.approveContract(contractDTO);
            return ResponseEntity.ok(contractService.getAllContracts());
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping
    @PreAuthorize("@customMethodSecurity.hasAnyRoleAuthority('ADMIN','EMPLOYEE')")
    public ResponseEntity<?> updateContract(@RequestBody ContractDTO contractDTO) {
        try {
            contractService.updateContract(contractDTO);
            return ResponseEntity.ok(contractService.getAllContracts());
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping("/{contractId}")
    @PreAuthorize("@customMethodSecurity.hasAnyRoleAuthority('ADMIN','EMPLOYEE')")
    public ResponseEntity<?> deleteContract(@PathVariable String contractId) {
        try {
            contractService.deleteContract(contractId);
            return ResponseEntity.ok(contractService.getAllContracts());
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

        /*


    User Requests


     */

    @GetMapping
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<?> getContractByUsername() {
        try {
            return ResponseEntity.ok(contractService.getContractByUsername());
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }



}
















