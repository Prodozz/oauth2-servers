package com.example.resourceserver.contract;

import com.example.resourceserver.car.CarEntity;
import com.example.resourceserver.contractPayment.Contract_PaymentStatusEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToOne;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@Data

public class ContractDTO {

    private String contractId;
    private String rentStartDate;
    private String rentEndDate;
    private String contractDate;
    private Double monthlyRate;
    private String contractTerm;
    private Boolean isActive;
    private String employeeId;
    private String carId;
    private String contractPaymentStatusId;
    private String username;

}
