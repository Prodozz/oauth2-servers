package com.example.resourceserver.contract;

import com.example.resourceserver.car.CarEntity;
import com.example.resourceserver.contractPayment.Contract_PaymentStatusEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.Hibernate;

import java.util.Objects;
import java.util.UUID;

@Entity
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ContractEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private UUID contractId;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private String rentStartDate;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private String rentEndDate;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private String contractDate;
    private Double monthlyRate;
    private String contractTerm;
    private Boolean isActive;
    private String username;
    private String employeeId;
    private String isApproved;


    @ManyToOne
    @JoinColumn(name = "carId")
    private CarEntity car;

    @OneToOne(mappedBy = "contract")
    private Contract_PaymentStatusEntity contractPaymentStatusEntity;


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        ContractEntity that = (ContractEntity) o;
        return contractId != null && Objects.equals(contractId, that.contractId);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
