package com.example.resourceserver.contract;

import com.example.resourceserver.car.CarCRUDRepository;
import com.example.resourceserver.car.CarEntity;
import com.example.resourceserver.contractPayment.Contract_PaymentStatusCRUDRepository;
import com.example.resourceserver.contractPayment.Contract_PaymentStatusEntity;
import lombok.AllArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class ContractService {

    ContractMapper contractMapper;
    ContractCRUDRepository contractCRUDRepository;
    CarCRUDRepository carCRUDRepository;
    Contract_PaymentStatusCRUDRepository contractPaymentStatusCRUDRepository;

    public List<ContractDTO> getAllContracts() {

        List<ContractEntity> contractEntities = (List<ContractEntity>) contractCRUDRepository.findAll();
        return contractEntities.stream().map(contractEntity -> contractMapper.toDto(contractEntity)).collect(Collectors.toList());

    }

    public ContractDTO getContractById(String contractId) {
        return contractMapper.toDto(contractCRUDRepository.findById(UUID.fromString(contractId))
                .orElseThrow(() -> new NoSuchElementException("Error!")));
    }

    public ContractDTO getContractByEmployeeId(String employeeId) {
        return contractMapper.toDto(contractCRUDRepository.findByEmployeeId(employeeId)
                .orElseThrow(() -> new NoSuchElementException("Error!")));
    }

    public List<ContractDTO> getContractByUsername() {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        List<ContractEntity> contractEntities = contractCRUDRepository.findByUsername(username);

        return contractEntities.stream().map(contractEntity -> contractMapper.toDto(contractEntity))
                .collect(Collectors.toList());
    }


    public void suggestContract(ContractSuggestionDTO contractDTO) throws Exception {

//        if (contractDTO.getCarId() == null) {
//            throw new Exception("kein Vertrag ohne Auto!");
//        }

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        ContractEntity contractEntity = new ContractEntity();
        contractMapper.mapToEntity(contractDTO, contractEntity);

        CarEntity carEntity = null;

        if (contractDTO.getCarId() != null) {
        carEntity = carCRUDRepository.findByCarId(UUID.fromString(contractDTO.getCarId()))
                .orElse(null);
        }

        contractEntity.setCar(carEntity);
        contractEntity.setUsername(authentication.getName());

        contractCRUDRepository.save(contractEntity);

    }

    public void approveContract(ContractApprovalDTO contractDTO) throws Exception {

//        if (contractDTO.getCarId() == null) {
//            throw new Exception("kein Vertrag ohne Auto!");
//        }

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        ContractEntity contractEntity = contractCRUDRepository.findById(UUID.fromString(contractDTO.getContractId()))
                .orElseThrow(() -> new NoSuchElementException("Error!"));
        contractMapper.mapToEntity(contractDTO, contractEntity);

        Contract_PaymentStatusEntity contractPaymentStatusEntity = null;

        if (contractDTO.getContractPaymentStatusId() != null) {
            contractPaymentStatusEntity = contractPaymentStatusCRUDRepository
                    .findByContractPaymentStatusId(UUID.fromString(contractDTO.getContractPaymentStatusId()))
                    .orElse(null);
        }


        contractEntity.setContractPaymentStatusEntity(contractPaymentStatusEntity);
        contractEntity.setEmployeeId(authentication.getName());

        contractCRUDRepository.save(contractEntity);

    }

    public void deleteContract(String contractId) {
        ContractEntity contractEntity = contractCRUDRepository
                .findByContractId(UUID.fromString(contractId))
                .orElseThrow(() -> new NoSuchElementException("Error!"));

            CarEntity car = contractEntity.getCar();
            car.getContractEntities().remove(contractEntity);
            carCRUDRepository.save(car);

            Contract_PaymentStatusEntity contractPaymentStatusEntity = contractEntity.getContractPaymentStatusEntity();
            contractPaymentStatusEntity.setContract(null);
            contractPaymentStatusCRUDRepository.save(contractPaymentStatusEntity);


        contractCRUDRepository.deleteByContractId(UUID.fromString(contractId));
    }


    public void updateContract(ContractDTO contractDTO) {

        ContractEntity contractEntity = contractCRUDRepository
                .findByContractId(UUID.fromString(contractDTO.getContractId()))
                .orElseThrow(() -> new NoSuchElementException("Error!"));
        contractMapper.mapToEntity(contractDTO, contractEntity);


        CarEntity carEntity = carCRUDRepository.findByCarId(UUID.fromString(contractDTO.getCarId()))
                .orElse(null);

        contractEntity.setCar(carEntity);

        contractCRUDRepository.save(contractEntity);
    }
}
