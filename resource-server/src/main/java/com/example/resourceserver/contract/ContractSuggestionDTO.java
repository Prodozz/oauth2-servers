package com.example.resourceserver.contract;


import lombok.Data;

@Data
public class ContractSuggestionDTO {

    private String rentStartDate;
    private String rentEndDate;
    private String contractDate;
    private Double monthlyRate;
    private String contractTerm;
    private String carId;



}
