package com.example.resourceserver.contractPayment;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface Contract_PaymentStatusCRUDRepository extends CrudRepository<Contract_PaymentStatusEntity, UUID> {

    Optional<Contract_PaymentStatusEntity> findByContractPaymentStatusId (UUID uuid);
    List<Contract_PaymentStatusEntity> findByUsername (String username);
    void deleteByContractPaymentStatusId (UUID uuid);

}
