package com.example.resourceserver.contractPayment;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping(path = "/api/contract_paymentstatus")
@AllArgsConstructor
@PreAuthorize("isAuthenticated()")
public class Contract_PaymentStatusController {

    final Contract_PaymentStatusService contract_PaymentStatusService;
    final Contract_PaymentStatusCRUDRepository contract_PaymentStatusCRUDRepository;

    @GetMapping("/getAll")
    @PreAuthorize("@customMethodSecurity.hasAnyRoleAuthority('ADMIN','EMPLOYEE')")
    public ResponseEntity<?> getAllContract_PaymentStatus() {
        try {
            return ResponseEntity.ok(contract_PaymentStatusService.getAllContract_PaymentStatus());
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/{contract_PaymentStatusId}")
    @PreAuthorize("@customMethodSecurity.hasAnyRoleAuthority('ADMIN','EMPLOYEE')")
    public ResponseEntity<?> getContract_PaymentStatus(@PathVariable String contract_PaymentStatusId) {
        try {
            return ResponseEntity.ok(contract_PaymentStatusService.getContract_PaymentStatusById(contract_PaymentStatusId));
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }


    @PostMapping
    @PreAuthorize("@customMethodSecurity.hasAnyRoleAuthority('EMPLOYEE', 'ADMIN')")
    public ResponseEntity<?> createContract_PaymentStatus(@RequestBody Contract_PaymentStatusDTO contract_PaymentStatusDTO) {
        try {
            contract_PaymentStatusService.createContract_PaymentStatus(contract_PaymentStatusDTO);
            return ResponseEntity.ok(contract_PaymentStatusService.getAllContract_PaymentStatus());
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping
    @PreAuthorize("@customMethodSecurity.hasAnyRoleAuthority('ADMIN','EMPLOYEE')")
    public ResponseEntity<?> updateContract_PaymentStatus(@RequestBody Contract_PaymentStatusDTO contract_PaymentStatusDTO) {
        try {
            contract_PaymentStatusService.updateContract_PaymentStatus(contract_PaymentStatusDTO);
            return ResponseEntity.ok(contract_PaymentStatusService.getAllContract_PaymentStatus());
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping("/{contract_PaymentStatusId}")
    @PreAuthorize("@customMethodSecurity.hasAnyRoleAuthority('ADMIN','EMPLOYEE')")
    public ResponseEntity<?> deleteContract_PaymentStatus(@PathVariable String contract_PaymentStatusId) {
        try {
            contract_PaymentStatusService.deleteContract_PaymentStatus(contract_PaymentStatusId);
            return ResponseEntity.ok(contract_PaymentStatusService.getAllContract_PaymentStatus());
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

        /*


    User Requests


     */

    @GetMapping
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<?> getContract_PaymentStatusByUsername() {
        try {
            return ResponseEntity.ok(contract_PaymentStatusService.getContract_PaymentStatusByUsername());
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }


}
















