package com.example.resourceserver.contractPayment;

import com.example.resourceserver.contract.ContractEntity;
import com.example.resourceserver.enums.enums.IntervalOfPayments;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToOne;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@Data
public class Contract_PaymentStatusDTO {
    private String contractPaymentStatusId;
    private String intervalOfPayments;
    private String intervalRate;
    private String contractId;
    private String username;


}
