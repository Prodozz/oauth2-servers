package com.example.resourceserver.contractPayment;

import com.example.resourceserver.contract.ContractEntity;
import com.example.resourceserver.enums.enums.IntervalOfPayments;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.Hibernate;

import java.util.Objects;
import java.util.UUID;

@Entity
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Table(name = "Contract_PaymentStatusEntity")
public class Contract_PaymentStatusEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private UUID contractPaymentStatusId;
    @Enumerated(EnumType.STRING)
    private IntervalOfPayments intervalOfPayments;
    private String intervalRate;
    private String username;

    @OneToOne
    @JoinColumn(name = "contractId")
    private ContractEntity contract;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Contract_PaymentStatusEntity that = (Contract_PaymentStatusEntity) o;
        return contractPaymentStatusId != null && Objects.equals(contractPaymentStatusId, that.contractPaymentStatusId);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
