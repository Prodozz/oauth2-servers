package com.example.resourceserver.contractPayment;

import com.example.resourceserver.contract.ContractCRUDRepository;
import com.example.resourceserver.contract.ContractEntity;
import lombok.AllArgsConstructor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class Contract_PaymentStatusService {

    private Contract_PaymentStatusMapper contractPaymentStatusMapper;
    private Contract_PaymentStatusCRUDRepository contractPaymentStatusCRUDRepository;
    private ContractCRUDRepository contractCRUDRepository;

    public List<Contract_PaymentStatusDTO> getAllContract_PaymentStatus() {

        List<Contract_PaymentStatusEntity> contractPaymentStatusEntities =
                (List<Contract_PaymentStatusEntity>) contractPaymentStatusCRUDRepository.findAll();

        return contractPaymentStatusEntities.stream().map(contractPaymentStatusEntity ->
                contractPaymentStatusMapper.toDto(contractPaymentStatusEntity)).collect(Collectors.toList());
    }


    public Contract_PaymentStatusDTO getContract_PaymentStatusById(String contract_paymentstatusId) {

        Contract_PaymentStatusEntity contractPaymentStatusEntity = contractPaymentStatusCRUDRepository
                .findByContractPaymentStatusId(UUID.fromString(contract_paymentstatusId))
                .orElseThrow(() -> new NoSuchElementException("Error!"));

        return contractPaymentStatusMapper.toDto(contractPaymentStatusEntity);

    }

    public List<Contract_PaymentStatusDTO> getContract_PaymentStatusByUsername() {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();

        List<Contract_PaymentStatusEntity> contractPaymentStatusEntities = contractPaymentStatusCRUDRepository
                .findByUsername(username);


        return contractPaymentStatusEntities.stream().map(contractPaymentStatusEntity ->
                contractPaymentStatusMapper.toDto(contractPaymentStatusEntity)).collect(Collectors.toList());

    }


    public void createContract_PaymentStatus(Contract_PaymentStatusDTO contract_paymentstatusDTO) {

        Contract_PaymentStatusEntity contractPaymentStatusEntity = contractPaymentStatusMapper.toEntity(contract_paymentstatusDTO);
        contractPaymentStatusMapper.updateEntity(contract_paymentstatusDTO, contractPaymentStatusEntity);

        ContractEntity contractEntity = null;

        if (contract_paymentstatusDTO.getContractId() != null) {
            contractEntity = contractCRUDRepository
                    .findByContractId(UUID.fromString(contract_paymentstatusDTO.getContractId()))
                    .orElseThrow(() -> new NoSuchElementException("Error"));
        }

        contractPaymentStatusEntity.setContract(contractEntity);

        contractPaymentStatusCRUDRepository.save(contractPaymentStatusEntity);

    }

    public void deleteContract_PaymentStatus(String contract_paymentstatusId) throws NoSuchElementException {

        Contract_PaymentStatusEntity contractPaymentStatusEntity = contractPaymentStatusCRUDRepository
                .findByContractPaymentStatusId(UUID.fromString(contract_paymentstatusId))
                .orElseThrow(() -> new NoSuchElementException("Error"));


        if (contractPaymentStatusEntity.getContract() != null) {
            ContractEntity contract = contractCRUDRepository
                    .findByContractId(contractPaymentStatusEntity.getContract().getContractId())
                    .orElseThrow(() -> new NoSuchElementException("Error!"));
            contract.setContractPaymentStatusEntity(null);
            contractCRUDRepository.save(contract);
        }

        contractPaymentStatusCRUDRepository.deleteByContractPaymentStatusId(UUID.fromString(contract_paymentstatusId));

    }

    public void updateContract_PaymentStatus(Contract_PaymentStatusDTO contract_paymentstatusDTO) {

        Contract_PaymentStatusEntity contractPaymentStatusEntity = contractPaymentStatusCRUDRepository
                .findByContractPaymentStatusId(UUID.fromString(contract_paymentstatusDTO.getContractPaymentStatusId()))
                .orElseThrow(() -> new NoSuchElementException("Error!"));

        contractPaymentStatusMapper.updateEntity(contract_paymentstatusDTO, contractPaymentStatusEntity);

        ContractEntity contractEntity = contractCRUDRepository
                .findByContractId(UUID.fromString(contract_paymentstatusDTO.getContractId()))
                .orElse(null);

        contractPaymentStatusEntity.setContract(contractEntity);

        contractPaymentStatusCRUDRepository.save(contractPaymentStatusEntity);

    }
}
