package com.example.resourceserver.enums;

import com.example.resourceserver.enums.enums.Status;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/api/enums")
@PreAuthorize("isAuthenticated()")
public class EnumsController {

    @GetMapping("/status")
    public ResponseEntity<?> getStatusEnum(){

        List<Status> status = Arrays.asList(Status.values());
        return ResponseEntity.ok(status);
    }
}
