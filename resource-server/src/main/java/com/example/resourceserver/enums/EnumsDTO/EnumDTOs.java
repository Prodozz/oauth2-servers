package com.example.resourceserver.enums.EnumsDTO;

import com.example.resourceserver.enums.enums.Status;
import lombok.Getter;
import lombok.Setter;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
public class EnumDTOs {

    Set<String> status = new HashSet<>();


    public EnumDTOs (){
        Arrays.stream(Status.values()).forEach(stat -> status.add(stat.getName()));
    }
}
