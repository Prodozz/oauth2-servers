package com.example.resourceserver.enums.enums;

public enum Gender {
    MALE,
    FEMALE,
    NON_BINARY;

}
