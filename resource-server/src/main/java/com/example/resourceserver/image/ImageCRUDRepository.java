package com.example.resourceserver.image;


import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface ImageCRUDRepository extends CrudRepository<ImageEntity, UUID> {

    Optional<ImageEntity> findByName(String name);

}
