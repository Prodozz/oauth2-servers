package com.example.resourceserver.image;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/api/image")
@RequiredArgsConstructor
public class ImageController {

    private final ImageService imageService;

    @PostMapping("/upload")
    public ResponseEntity<?> uploadImage(@RequestParam("image") MultipartFile file) {
        try {
            imageService.saveImage(file);
           return ResponseEntity.ok(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/getAll")
    public ResponseEntity<?> getAllImageDetails() {
        try {
            return ResponseEntity.ok(imageService.getAllImageDetails());
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }


    @GetMapping("/info/id/{imageId}")
    public ResponseEntity<?> getImageDetailsById(@PathVariable String imageId) {
        try {
           return ResponseEntity.ok(imageService.getImageDetailsById(imageId));
        } catch (Exception e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
    @GetMapping("/info/{name}")
    public ResponseEntity<?> getImageDetailsByName(@PathVariable String name) {
        try {
           return ResponseEntity.ok(imageService.getImageDetailsByName(name));
        } catch (Exception e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/id/{imageId}")
    public ResponseEntity<?> getImageById(@PathVariable String imageId) {
        try {
            return ResponseEntity.ok(imageService.getImageById(imageId));
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/{name}")
    public ResponseEntity<?> getImageByName(@PathVariable String name) {
        try {
            return ResponseEntity.ok(imageService.getImageByName(name));
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }


}
