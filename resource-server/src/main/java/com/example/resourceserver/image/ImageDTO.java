package com.example.resourceserver.image;

import jakarta.persistence.Column;
import lombok.Data;

import java.util.UUID;

@Data
public class ImageDTO {

    private UUID imageId;
    private String name;
    private String type;
    private byte[] image;

}
