package com.example.resourceserver.image;


import jakarta.persistence.*;
import lombok.*;

import java.util.UUID;

@Entity
@Table(name = "image")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ImageEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private UUID imageId;
    @Column(unique = true)
    private String name;
    private String type;
    @Column(name = "image", nullable = false, length = 100000)
    private byte[] image;



}
