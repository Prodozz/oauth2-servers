package com.example.resourceserver.image;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;
import org.springframework.web.multipart.MultipartFile;

import java.util.UUID;

@Mapper(componentModel = "spring", uses = {UUID.class}, unmappedTargetPolicy = ReportingPolicy.WARN)
public interface ImageMapper {

    @Mapping(target = "imageId", ignore = true)
    @Mapping(target = "image", ignore = true)
    @Mapping(target = "name", ignore = true)
    @Mapping(expression = "java(file.getContentType())", target = "type")
    ImageEntity toEntity(MultipartFile file);

    ImageDTO toDto(ImageEntity imageEntity);


}
