package com.example.resourceserver.image;

import lombok.RequiredArgsConstructor;
import org.apache.commons.imaging.ImageReadException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ImageService {

    private final ImageCRUDRepository imageCRUDRepository;
    private final ImageMapper imageMapper;

    public void saveImage(MultipartFile file) throws IOException {
        String timeStamp = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss").format(new Date());
        String name = SecurityContextHolder.getContext().getAuthentication().getName();

        ImageEntity imageEntity = imageMapper.toEntity(file);

        imageEntity.setImage(ImageUtility.compressImage(file));
        imageEntity.setName(name + "_" + file.getOriginalFilename() + "_" + timeStamp);

        imageCRUDRepository.save(imageEntity);

    }

    public ImageDTO getImageDetailsById(String imageId) throws IOException, ImageReadException {

        ImageEntity imageEntity = imageCRUDRepository.findById(UUID.fromString(imageId))
                .orElseThrow(() -> new NoSuchElementException("Error!"));

        return imageMapper.toDto(imageEntity);
    }
    public ImageDTO getImageDetailsByName(String name) throws IOException, ImageReadException {


        ImageEntity imageEntity = imageCRUDRepository.findByName(name)
                .orElseThrow(() -> new NoSuchElementException("Error!"));

        return imageMapper.toDto(imageEntity);
    }


    public byte[] getImageById(String imageId) throws IOException, ImageReadException {

        ImageEntity imageEntity = imageCRUDRepository.findById(UUID.fromString(imageId))
                .orElseThrow(() -> new NoSuchElementException("Error!"));

        return imageEntity.getImage();

    }
    public byte[] getImageByName(String name) throws IOException, ImageReadException {

        ImageEntity imageEntity = imageCRUDRepository.findByName(name)
                .orElseThrow(() -> new NoSuchElementException("Error!"));

        return imageEntity.getImage();

    }

    public List<ImageDTO> getAllImageDetails(){
        List<ImageEntity> images = (List<ImageEntity>) imageCRUDRepository.findAll();
        return images.stream().map(imageMapper::toDto).collect(Collectors.toList());
    }


}

