package com.example.resourceserver.motorcarAccident;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface MotorcarAccidentCRUDRepository extends CrudRepository<MotorcarAccidentEntity, UUID> {

    Optional<MotorcarAccidentEntity> findByMotorcarAccidentId (UUID uuid);
    Optional<MotorcarAccidentEntity> findByEmployeeId(String employeeId);
    List<MotorcarAccidentEntity> findByUsername(String customerId);
    void deleteByMotorcarAccidentId (UUID uuid);

}
