package com.example.resourceserver.motorcarAccident;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping(path = "/api/motorcarAccident")
@AllArgsConstructor
public class MotorcarAccidentController {

    final MotorcarAccidentService motorcarAccidentService;

    @GetMapping("/getAll")
    @PreAuthorize("@customMethodSecurity.hasAnyRoleAuthority('ADMIN', 'EMPLOYEE')")
    public ResponseEntity<?> getAllMotorcarAccidents() {
        try {
            return ResponseEntity.ok(motorcarAccidentService.getAllMotorcarAccidents());
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/{motorcarAccidentId}")
    @PreAuthorize("@customMethodSecurity.hasAnyRoleAuthority('EMPLOYEE', 'ADMIN')")
    public ResponseEntity<?> getMotorcarAccidentById(@PathVariable String motorcarAccidentId) {
        try {
            return ResponseEntity.ok(motorcarAccidentService.getMotorcarAccidentById(motorcarAccidentId));
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<?> getMotorcarAccidentByUsername() {
        try {
            return ResponseEntity.ok(motorcarAccidentService.getByUsername());
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/employee/{employeeId}")
    @PreAuthorize("@customMethodSecurity.hasAnyRoleAuthority('ADMIN', 'EMPLOYEE')")
    public ResponseEntity<?> getMotorcarAccidentByEmployeeId(@PathVariable String employeeId) {
        try {
            return ResponseEntity.ok(motorcarAccidentService.getByEmployeeId(employeeId));
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<?> createMotorcarAccident(@RequestBody MotorcarAccidentDTO motorcarAccidentDTO) {
        try {
            motorcarAccidentService.createMotorcarAccident(motorcarAccidentDTO);
            return ResponseEntity.ok(motorcarAccidentService.getAllMotorcarAccidents());
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
    @PutMapping
    @PreAuthorize("@customMethodSecurity.hasAnyRoleAuthority('ADMIN','EMPLOYEE')")
    public ResponseEntity<?> updateMotorcarAccident(@RequestBody MotorcarAccidentDTO motorcarAccidentDTO) {
        try {
            motorcarAccidentService.updateMotorcarAccident(motorcarAccidentDTO);
            return ResponseEntity.ok(motorcarAccidentService.getAllMotorcarAccidents());
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping("/{motorcarAccidentId}")
    @PreAuthorize("@customMethodSecurity.hasAnyRoleAuthority('ADMIN', 'EMPLOYEE')")
    public ResponseEntity<?> deleteMotorcarAccident(@PathVariable String motorcarAccidentId) {
        try {
            motorcarAccidentService.deleteMotorcarAccident(motorcarAccidentId);
            return ResponseEntity.ok(motorcarAccidentService.getAllMotorcarAccidents());
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

        /*


    User Requests


     */



}
















