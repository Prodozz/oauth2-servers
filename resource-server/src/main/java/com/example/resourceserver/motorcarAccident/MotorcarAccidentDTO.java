package com.example.resourceserver.motorcarAccident;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MotorcarAccidentDTO {

    private String motorcarAccidentId;
    private String accidentHeader;
    private String accidentDescriptionText;
    private String username;
    private String employeeId;

}
