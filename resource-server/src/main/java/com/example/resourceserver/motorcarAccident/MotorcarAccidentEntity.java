package com.example.resourceserver.motorcarAccident;

import com.example.resourceserver.car.CarEntity;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class MotorcarAccidentEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private UUID motorcarAccidentId;
    private String accidentHeader;
    private String accidentDescriptionText;
    private String username;
    private String employeeId;

    @OneToOne
    @JoinColumn(name = "carId")
    private CarEntity car;

}
