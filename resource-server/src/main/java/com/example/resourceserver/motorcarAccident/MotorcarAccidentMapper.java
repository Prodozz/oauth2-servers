package com.example.resourceserver.motorcarAccident;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

import java.util.UUID;

@Mapper(componentModel = "spring", uses = {UUID.class})
public interface MotorcarAccidentMapper {

    MotorcarAccidentMapper MOTORCAR_ACCIDENT_MAPPER = Mappers.getMapper(MotorcarAccidentMapper.class);


    @Mapping(source= "motorcarAccidentEntity.employeeId", target = "employeeId")
    MotorcarAccidentDTO toDto(MotorcarAccidentEntity motorcarAccidentEntity);

    @Mapping(target = "motorcarAccidentId", ignore = true)
    @Mapping(target = "username", ignore = true)
    @Mapping(target = "employeeId", ignore = true)
    MotorcarAccidentEntity toEntity(MotorcarAccidentDTO motorcarAccidentDTO);

    @Mapping(target = "motorcarAccidentId", ignore = true)
    void updateEntity(MotorcarAccidentDTO motorcarAccidentDTO, @MappingTarget MotorcarAccidentEntity motorcarAccidentEntity);


}
