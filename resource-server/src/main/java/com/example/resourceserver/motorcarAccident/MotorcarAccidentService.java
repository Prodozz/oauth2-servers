package com.example.resourceserver.motorcarAccident;

import com.example.resourceserver.security.CustomMethodSecurity;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class MotorcarAccidentService {

    private final MotorcarAccidentCRUDRepository motorcarAccidentCRUDRepository;
    private final MotorcarAccidentMapper motorcarAccidentMapper;
    private final CustomMethodSecurity customMethodSecurity;

    public List<MotorcarAccidentDTO> getAllMotorcarAccidents() {

        List<MotorcarAccidentEntity> motorcarAccidentEntities =
                (List<MotorcarAccidentEntity>) motorcarAccidentCRUDRepository.findAll();

        return motorcarAccidentEntities.stream()
                .map(motorcarAccidentMapper::toDto).collect(Collectors.toList());

    }


    public MotorcarAccidentDTO getMotorcarAccidentById(String motorcaraccidentId) throws Exception {

        existsById(motorcaraccidentId);

        MotorcarAccidentEntity motorcarAccidentEntity = motorcarAccidentCRUDRepository
                .findByMotorcarAccidentId(UUID.fromString(motorcaraccidentId))
                .orElseThrow(() -> new NoSuchElementException("Error!"));

        return motorcarAccidentMapper.toDto(motorcarAccidentEntity);

    }
    public List<MotorcarAccidentDTO> getByUsername() {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        String username = authentication.getName();
        List<MotorcarAccidentEntity> motorcarAccidentEntities = motorcarAccidentCRUDRepository.findByUsername(username);

        return motorcarAccidentEntities.stream()
                .map(motorcarAccidentMapper::toDto).collect(Collectors.toList());

    }

    public MotorcarAccidentDTO getByEmployeeId(String employeeId) {

        MotorcarAccidentEntity motorcarAccidentEntity = motorcarAccidentCRUDRepository
                .findByEmployeeId(employeeId)
                .orElseThrow(() -> new NoSuchElementException("Error!"));

        return motorcarAccidentMapper.toDto(motorcarAccidentEntity);

    }


    public void createMotorcarAccident(MotorcarAccidentDTO motorcaraccidentDTO) {

//        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        MotorcarAccidentEntity motorcarAccidentEntity = motorcarAccidentMapper.toEntity(motorcaraccidentDTO);

//        if (customMethodSecurity.isCustomer()) {
//            motorcarAccidentEntity.setUsername(authentication.getName());
//        }
//        if (customMethodSecurity.isAdmin() || customMethodSecurity.isEmployee()) {
//            motorcarAccidentEntity.setEmployeeId(motorcarAccidentEntity.getEmployeeId());
//        }

        motorcarAccidentCRUDRepository.save(motorcarAccidentEntity);

    }

    public void deleteMotorcarAccident(String motorcaraccidentId) throws Exception {

        existsById(motorcaraccidentId);

        motorcarAccidentCRUDRepository.deleteById(UUID.fromString(motorcaraccidentId));

    }

    public void updateMotorcarAccident(MotorcarAccidentDTO motorcaraccidentDTO) throws Exception {

        existsById(motorcaraccidentDTO.getMotorcarAccidentId());

        MotorcarAccidentEntity motorcarAccidentEntity = motorcarAccidentCRUDRepository
                .findByMotorcarAccidentId(UUID.fromString(motorcaraccidentDTO.getMotorcarAccidentId()))
                .orElseThrow(() -> new NoSuchElementException("Error!"));

        motorcarAccidentMapper.updateEntity(motorcaraccidentDTO, motorcarAccidentEntity);

        motorcarAccidentCRUDRepository.save(motorcarAccidentEntity);

    }

    private void existsById(String motorcarAccidentId)throws Exception {
        if (!motorcarAccidentCRUDRepository.existsById(UUID.fromString(motorcarAccidentId))) {
            throw new Exception("Error!");
        }
    }
}
