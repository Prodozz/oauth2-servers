package com.example.resourceserver.rating;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface RatingCRUDRepository extends CrudRepository<RatingEntity, UUID> {

    List<RatingEntity> findByUsername(String username);

}
