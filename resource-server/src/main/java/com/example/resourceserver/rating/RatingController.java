package com.example.resourceserver.rating;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping(path = "/api/rating")
@AllArgsConstructor
public class RatingController {

    final RatingService ratingService;
    final RatingCRUDRepository ratingCRUDRepository;

    @GetMapping("/getAll")
    @PreAuthorize("@customMethodSecurity.isAdmin()")
    public ResponseEntity<?> getAllRatings() {
        try {
            return ResponseEntity.ok(ratingService.getAllRatings());
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/{ratingId}")
    @PreAuthorize("@customMethodSecurity.hasAnyRoleAuthority('ADMIN', 'EMPLOYEE')")
    public ResponseEntity<?> getRatingById(@PathVariable String ratingId) {
        try {
            return ResponseEntity.ok(ratingService.getRating(ratingId));
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<?> getRating() {
        try {
            return ResponseEntity.ok(ratingService.getRatingByUsername());
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<?> createRating(@RequestBody RatingDTO ratingDTO) {
        try {
            ratingService.createRating(ratingDTO);
            return ResponseEntity.ok(ratingService.getAllRatings());
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
    @PutMapping
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<?> updateRating(@RequestBody RatingDTO ratingDTO) {
        try {
            ratingService.updateRating(ratingDTO);
            return ResponseEntity.ok(ratingService.getAllRatings());
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping("/{ratingId}")
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<?> deleteRating(@PathVariable String ratingId) {
        try {
            ratingService.deleteRating(ratingId);
            return ResponseEntity.ok(ratingService.getAllRatings());
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

        /*


    User Requests


     */



}
















