package com.example.resourceserver.rating;


import com.example.resourceserver.car.CarEntity;
import jakarta.persistence.Column;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@Data
public class RatingDTO {

    private String ratingId;
    private int rating;
    private String feedbackText;
    private String username;
    private String carId;

}
