package com.example.resourceserver.rating;

import com.example.resourceserver.car.CarEntity;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@Entity
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class RatingEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private UUID ratingId;
    private Integer rating;
    private String feedbackText;
    private String username;


    @ManyToOne
    @JoinColumn(name = "carId")
    private CarEntity car;
}
