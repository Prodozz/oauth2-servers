package com.example.resourceserver.rating;

import com.example.resourceserver.car.CarCRUDRepository;
import com.example.resourceserver.car.CarEntity;
import lombok.AllArgsConstructor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class RatingService {

    RatingCRUDRepository ratingCRUDRepository;
    CarCRUDRepository carCRUDRepository;
    RatingMapper ratingMapper;

    public List<RatingDTO> getAllRatings() {

        List<RatingEntity> ratingEntities = (List<RatingEntity>) ratingCRUDRepository.findAll();
        return ratingEntities.stream().map(ratingMapper::toDto).collect(Collectors.toList());

    }


    public RatingDTO getRating(String ratingId) throws Exception {

        existsById(ratingId);

        RatingEntity ratingService = ratingCRUDRepository
                .findById(UUID.fromString(ratingId))
                .orElseThrow(() -> new NoSuchElementException("Error!"));

        return ratingMapper.toDto(ratingService);

    }

    public List<RatingDTO> getRatingByUsername() {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();

        List<RatingEntity> ratingEntities = ratingCRUDRepository
                .findByUsername(username);

        return ratingEntities.stream().map(ratingMapper::toDto).collect(Collectors.toList());

    }

    public void createRating(RatingDTO ratingDTO) {

        RatingEntity ratingEntity = ratingMapper.toEntity(ratingDTO);
        ratingMapper.updateEntity(ratingDTO, ratingEntity);

        CarEntity carEntity = null;

        if (ratingDTO.getCarId() != null) {
            carEntity = carCRUDRepository
                    .findByCarId(UUID.fromString(ratingDTO.getCarId()))
                    .orElse(null);
        }
        ratingEntity.setCar(carEntity);
        ratingEntity.setUsername(SecurityContextHolder.getContext().getAuthentication().getName());

        ratingCRUDRepository.save(ratingEntity);


    }

    public void deleteRating(String ratingId) throws Exception {

        existsById(ratingId);

        RatingEntity ratingEntity = ratingCRUDRepository
                .findById(UUID.fromString(ratingId)).get();


        CarEntity carEntity = ratingEntity.getCar();

        carEntity.setRatingEntities(carEntity.getRatingEntities().stream()
                .filter(rating -> !rating.equals(ratingEntity)).collect(Collectors.toList()));

        carCRUDRepository.save(carEntity);


        ratingCRUDRepository.delete(ratingEntity);

    }

    public void updateRating(RatingDTO ratingDTO) throws Exception {

        existsById(ratingDTO.getRatingId());

        RatingEntity ratingEntity = ratingCRUDRepository.findById(UUID.fromString(ratingDTO.getRatingId()))
                .orElseThrow(() -> new NoSuchElementException("Error!"));
        ratingMapper.updateEntity(ratingDTO, ratingEntity);


        CarEntity carEntity = carCRUDRepository.findByCarId(UUID.fromString(ratingDTO.getCarId()))
                .orElse(null);

        ratingEntity.setCar(carEntity);

        ratingCRUDRepository.save(ratingEntity);


    }

    private void existsById (String ratingId) throws Exception {
        if (!ratingCRUDRepository.existsById(UUID.fromString(ratingId))) {
            throw new Exception("Error!");
        }
    }
}
