package com.example.resourceserver.security;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.server.resource.authentication.BearerTokenAuthentication;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class CustomMethodSecurity {

    public List<String> getTokenClaims(String... claim) {
        BearerTokenAuthentication authentication = (BearerTokenAuthentication) SecurityContextHolder.getContext().getAuthentication();

        List<String> list = new ArrayList<>();

        for (String s : claim) {
            list.add((String) authentication.getTokenAttributes().get(s));
        }
        return list;
    }

    @SuppressWarnings("unchecked")
    public List<String> getAuthoritiesClaim() {
        BearerTokenAuthentication authentication = (BearerTokenAuthentication) SecurityContextHolder.getContext().getAuthentication();

        return (List<String>) authentication.getTokenAttributes().get("authorities");

    }

    public Boolean hasAnyRoleAuthority(String... roles){
        for (String role : roles) {

            boolean check = getAuthoritiesClaim().contains("ROLE_" + role);

            if (check){
                return true;
            }
        }
        return false;
    }

    public Boolean isAdmin(){
        return hasAnyRoleAuthority("ADMIN");
    }

    public Boolean isEmployee(){
        return hasAnyRoleAuthority("EMPLOYEE");
    }

    public Boolean isCustomer(){
        return hasAnyRoleAuthority("CUSTOMER");
    }


}
