package com.example.resourceserver.security;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.SecurityFilterChain;

@Configuration(proxyBeanMethods = false)
@EnableWebSecurity
//@EnableMethodSecurity
@RequiredArgsConstructor
public class ResourceServerConfig {

    private final CorsCustomizer corsCustomizer;

    @Bean
    @Order(1)
    public SecurityFilterChain resourceServerSecurityFilterChain(HttpSecurity http) throws Exception {
//        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
//        OAuth2AccessToken accessToken = (OAuth2AccessToken) authentication.getDetails();



        http
                .csrf().disable()
                .authorizeHttpRequests(auth -> auth
//                        .anyRequest().authenticated()
                        .anyRequest().permitAll()
                )
                .oauth2ResourceServer(oauth2 -> oauth2.opaqueToken(
                        opaqueToken -> opaqueToken
                                .introspectionUri("http://localhost:8080/oauth2/introspect")
                                .introspectionClientCredentials("messaging-client", "{noop}secret")
                ));

//        http
//                .oauth2ResourceServer().jwt().jwtAuthenticationConverter(accessToken -> accessToken
//                        .getClaims().get("authorities")
//                );


//        http.oauth2ResourceServer(oauth2 -> oauth2.jwt(jwtConfigurer -> jwtConfigurer
//                .jwtAuthenticationConverter(source -> source.)))
        corsCustomizer.corsCustomizer(http);
        return http.build();
    }





}
