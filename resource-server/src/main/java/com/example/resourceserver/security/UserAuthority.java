package com.example.resourceserver.security;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.security.core.GrantedAuthority;

@AllArgsConstructor
@Getter
public enum UserAuthority implements GrantedAuthority {

    CUSTOMER("CUSTOMER"),
    EMPLOYEE("EMPLOYEE"),
    ADMIN("ADMIN");

    private final String authority;

    @Override
    public String getAuthority() {
        return authority;
    }

    public static UserAuthority fromStringToAuthority(String authorityString) {
        for (UserAuthority authority : UserAuthority.values()) {
            if (authority.getAuthority().equals(authorityString)) {
                return authority;
            }
        }
        throw new IllegalArgumentException("Error!");
    }
}
